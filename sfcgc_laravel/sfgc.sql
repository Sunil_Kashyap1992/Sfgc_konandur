-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `document_table`;
CREATE TABLE `document_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `documentId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `documentTitle` text COLLATE utf8_unicode_ci NOT NULL,
  `documentPath` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `document_table` (`id`, `documentId`, `documentTitle`, `documentPath`, `created_at`, `updated_at`) VALUES
(1,	'4869092F',	'fdsa',	'http://192.168.1.2:8000/sfgc/documents/4869092F.pdf',	'2018-03-14 12:33:37',	'2018-03-14 12:33:37'),
(2,	'162AA105',	'fsda',	'http://192.168.1.2:8000/sfgc/documents/162AA105.pdf',	'2018-03-15 04:57:40',	'2018-03-15 04:57:40'),
(3,	'8D5CBFBB',	'fsda',	'http://192.168.1.2:8000/sfgc/documents/8D5CBFBB.pdf',	'2018-03-15 04:57:51',	'2018-03-15 04:57:51'),
(4,	'6CFC6AC1',	'fadsgsdgds',	'http://192.168.1.2:8000/sfgc/documents/6CFC6AC1.pdf',	'2018-03-15 04:59:06',	'2018-03-15 04:59:06'),
(5,	'7BDBE2DA',	'saf',	'http://192.168.1.2:8000/sfgc/documents/7BDBE2DA.pdf',	'2018-03-15 07:11:09',	'2018-03-15 07:11:09'),
(6,	'29BF85F1',	'gsfddddddddddddddddd gfdddddddddddddddddddddddddddd',	'http://192.168.1.2:8000/sfgc/documents/29BF85F1.pdf',	'2018-03-15 10:39:20',	'2018-03-15 10:39:20');

DROP TABLE IF EXISTS `flash_news_table`;
CREATE TABLE `flash_news_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `flashNewsId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flashNewsTitle` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `flash_news_table` (`id`, `flashNewsId`, `flashNewsTitle`, `created_at`, `updated_at`) VALUES
(1,	'2F4CBB8B',	'Testsf',	'2018-03-15 04:34:59',	'2018-03-15 04:34:59'),
(2,	'D68C5055',	'Test',	'2018-03-15 04:45:21',	'2018-03-15 04:45:21'),
(3,	'38246C63',	'safffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff safvhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh sfajhvvvvvvvvvvvvvvvvvvvvvvvvfdsavvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv',	'2018-03-15 04:56:53',	'2018-03-15 04:56:53'),
(4,	'F53A5963',	'sfdsfds',	'2018-03-15 10:08:14',	'2018-03-15 10:08:14'),
(5,	'BB4D967C',	'sfdsfdssddsf',	'2018-03-15 10:09:00',	'2018-03-15 10:09:00');

DROP TABLE IF EXISTS `gallery_table`;
CREATE TABLE `gallery_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `galleryId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagePath` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `gallery_table` (`id`, `galleryId`, `imagePath`, `created_at`, `updated_at`) VALUES
(1,	'12340',	'https://sfgckonandur.in/sfgc/gallery/02.jpg',	'2018-03-14 11:35:59',	'2018-03-14 11:35:59'),
(2,	'12341',	'https://sfgckonandur.in/sfgc/gallery/03.jpg',	'2018-03-14 11:37:02',	'2018-03-14 11:37:02'),
(3,	'12342',	'https://sfgckonandur.in/sfgc/gallery/04.jpg',	'2018-03-14 12:12:25',	'2018-03-14 12:12:25'),
(4,	'12343',	'https://sfgckonandur.in/sfgc/gallery/05.jpg',	'2018-03-14 12:12:36',	'2018-03-14 12:12:36'),
(5,	'12344',	'https://sfgckonandur.in/sfgc/gallery/06.jpg',	'2018-03-14 12:15:45',	'2018-03-14 12:15:45'),
(6,	'12345',	'https://sfgckonandur.in/sfgc/gallery/07.jpg',	'2018-03-14 12:16:16',	'2018-03-14 12:16:16'),
(7,	'12346',	'https://sfgckonandur.in/sfgc/gallery/08.jpg',	'2018-03-14 12:16:34',	'2018-03-14 12:16:34'),
(8,	'12347',	'https://sfgckonandur.in/sfgc/gallery/09.jpg',	'2018-03-14 12:16:43',	'2018-03-14 12:16:43'),
(9,	'12348',	'https://sfgckonandur.in/sfgc/gallery/10.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(10,	'12349',	'https://sfgckonandur.in/sfgc/gallery/11.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(11,	'123410',	'https://sfgckonandur.in/sfgc/gallery/12.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(12,	'123411',	'https://sfgckonandur.in/sfgc/gallery/13.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(13,	'123412',	'https://sfgckonandur.in/sfgc/gallery/14.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(14,	'123413',	'https://sfgckonandur.in/sfgc/gallery/15.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(15,	'123414',	'https://sfgckonandur.in/sfgc/gallery/16.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(16,	'123415',	'https://sfgckonandur.in/fgc/gallery/17.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(17,	'123416',	'https://sfgckonandur.in/sfgc/gallery/18.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(18,	'123417',	'https://sfgckonandur.in/sfgc/gallery/19.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(19,	'123418',	'https://sfgckonandur.in/sfgc/gallery/20.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(20,	'123419',	'https://sfgckonandur.in/sfgc/gallery/21.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(21,	'123420',	'https://sfgckonandur.in/sfgc/gallery/22.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(22,	'123421',	'https://sfgckonandur.in/sfgc/gallery/23.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(23,	'123422',	'https://sfgckonandur.in/sfgc/gallery/24.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(24,	'123423',	'https://sfgckonandur.in/sfgc/gallery/25.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(25,	'123424',	'https://sfgckonandur.in/sfgc/gallery/26.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(26,	'123425',	'https://sfgckonandur.in/sfgc/gallery/27.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(27,	'123426',	'https://sfgckonandur.in/sfgc/gallery/28.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(28,	'123427',	'https://sfgckonandur.in/sfgc/gallery/29.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(29,	'123428',	'https://sfgckonandur.in/sfgc/gallery/30.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55'),
(30,	'123429',	'https://sfgckonandur.in/sfgc/gallery/31.jpg',	'2018-03-14 12:16:55',	'2018-03-14 12:16:55');

DROP TABLE IF EXISTS `login_table`;
CREATE TABLE `login_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_table_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `login_table` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Sfgc Konandur',	'sfgckonandur@gmail.com',	'$2y$10$Q6QdaHOrjIRc0KIjPxaTq.Iwu93PT0HtUedvKDFm/f04Ldg6tDZKa',	NULL,	'2018-03-14 08:33:40',	'2018-03-14 08:33:40');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_10_20_130427_login_table',	1),
('2018_03_13_150123_gallery_table',	1),
('2018_03_13_150150_flash_news_table',	1),
('2018_03_13_150250_document_table',	1);

-- 2019-07-25 14:54:58
