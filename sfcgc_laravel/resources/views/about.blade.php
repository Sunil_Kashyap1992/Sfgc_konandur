<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <meta charset="utf-8">
    <title>SFGC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="../sfgc/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sfgc/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../sfgc/css/jcarousel.css" rel="stylesheet" />
    <link href="../sfgc/css/flexslider.css" rel="stylesheet" />
    <link href="../sfgc/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="../sfgc/js/app.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="ng-cloak" ng-app="sfgc" ng-controller="Controller" background="../sfgc/img/background.jpg">
<div id="wrapper" style="background-color: transparent">

    <!-- start header -->
    <header style="background-color: transparent">
        <div class="navbar navbar-default navbar-static-top" style="background-color: transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"  href={{url('/')}}><img src="../sfgc/img/logo.png" alt="logo" style="height: 82px;width: 129px"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('motto')}}">Our Motto</a></li>
                        <li><a href="{{url('images')}}">Gallery</a></li>
                        <li><a href="{{url('document')}}">Documents</a></li>
                        <li><a href="">Quick Links</a>
                            <ul class="dropdown">
                                <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                                <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="pricing.html">Pricing</a></li>
                       <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </header><!-- end header -->
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">About Us</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content" style="background-color: transparent">
        <div class="container">

            <div class="row">
                <a href="{{url('ourInstitution')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                    <div class="card" style="width: 25rem;">
                        <img class="card-img-top" src="../sfgc/img/college.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title">About Our Institution</h4>
                            <p>National Education Society ®, Shivamogga, the parent body of sharavathi First Grade College,
                                Konandur is a famous name in the educational field.<a href="{{url('ourInstitution')}}" class="button">Read More</a></p>
                        </div>
                    </div>
                    </div>
                </a>
                <a href="{{url('ourCollege')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/institution.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">About Our College</h4>
                                <p>The senior citizens of the Kondnaur village, started Vidya
                                    Vardhaka Sangha in 1960 with the objective of starting a High School at Konandur.<a href="{{url('ourCollege')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{url('ourVision')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/09.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Our Vision</h4>
                                <p>To make the Institution one of the best places to seek knowledge and wisdom.<a href="{{url('ourVision')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row">
                <a href="{{url('ourMission')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/10.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Our Mission</h4>
                                <p>To provide higher education even to the girl students and the under privileged class of the rural society.<a href="{{url('ourMission')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{url('ourTeam')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/08.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">We are awesome Team</h4>
                                <p>The college makes sincere efforts to provide good teaching learning experiences to students.
                                    It also aims at improving the adequacy of the faculty .<a href="{{url('ourTeam')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{url('ourManagement')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/09.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Organization&Management</h4>
                                <p>Our management National Education Society ® Shivamogga, Karnataka State. is one of the reputed educational institutions
                                    in the state of Karnataka.<a href="{{url('ourManagement')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="row">
                <a href="{{url('ourObjectives')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/college.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Our Objectives</h4>
                                <p> To inspire the students to carry on our cultural heritage through participationin intellectual,cultural and sports activities. <a href="{{url('ourObjectives')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{url('ourTeaching')}}" style="color: inherit;">
                    <div class="col-sm-4 info-blocks">
                        <div class="card" style="width: 25rem;">
                            <img class="card-img-top" src="../sfgc/img/institution.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title">Teaching & Evalution</h4>
                                <p> The institution has a transparent admission process. The college makes sincere efforts to provide good teaching learning
                                    experiences to students.<a href="{{url('ourTeaching')}}" class="button">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            {{--<div class="about">
                <!-- Accordion starts -->
                <div class="panel-group" id="accordion-alt3">
                    <!-- Panel. Use "panel-XXX" class for different colors. Replace "XXX" with color. -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseFive-alt3">
                                    <i class="fa fa-angle-right"></i>About Our Institution
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="text-align: justify;">National Education Society ®, Shivamogga, the parent body of sharavathi First Grade College,
                                    Konandur is a famous name in the educational field of our state. It was established in 1946 by
                                    leading citizens of Shivamogga, with the concern of serving the cause of education for the
                                    children of Malanad Region</p>
                                <div id="ourInstitution" style="display: none;">
                                    <p style="text-align: justify">Late Mr. H.S.Rudrappa, Ex Minister of Karnataka was the founder president of the society,
                                        late Mr.S.V. Krishnamurthy Rao, Ex Deputy Speaker of Loksabha, Late Mr. S.R.Nagappa Shetty, Ex-M.L.A.,
                                        were the founder secretaries. Late Mr. Jayathirthachar an eminent educationist and leading advocate,
                                        Late Mr. P.Murudappa, Gandian and a leading  advocate, Late Sri Mahishi Narasimma Murthy, an eminent
                                        scholar and leading advocate, Late Mr.Basappa Patil, Freedom fighter, and Agriculturist and Late Mr.D.S.Dinakar, freedom fighter served
                                        as Presidents after Mr. H.S.Rudrappa. Late Mr. Girimaji N. Rajagopal, freedom fighter served as
                                        Secretary after Mr. S.R.Nagappa Shetty. Late Mr. Dodya Naik, Ex-M.L.A. served as Treasurer.
                                        It is a matter of great pride for the society to remember the visit of Late Dr. Babu Rajendra Prasad,
                                        Former President of India in-connection with the opening ceremony of national boys high school
                                        in 1949 and also the visit of Late Sri Lal Bahuddur Shastry, Former Prime Minister of India, in-connection with the
                                        foundation layingceremony of Kamala Nehru Memorial National College of Arts and Commerce for Women,
                                        Shivamogga on 24th January 1965. [ Contact no: 08182-273469,
                                        E mail ID -  nesedusociety@gmail.com , Web site: www.nes_shimoga.org ]
                                        National Education Society ® started its educational activities by starting a High School at Shivamogga in 1946.
                                        Now it is managing 42 educational institutions of various disciplines including Post Graduate course in Computer Applications, Business
                                        Administration, Computer Science, Engineering, I.C.S.C. and C.B.S.C. Residential Schools. Out of which 3-General colleges,
                                        5-Professional colleges, 2-Technical colleges and the rest are Pre-university colleges, High schools, Residential schools and Primary schools.
                                    </p>
                                </div>
                            <button  onclick="ourInstitution()"  id="toggle2" class="btn btn-color" style="background-color: white;color: #2d84d0;">Read More</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseSix-alt3">
                                    <i class="fa fa-angle-right"></i> About our College
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="text-align: justify;">National Education Society ® Shivamogga is the parent body of
                                    Sharavathi First Grade College, Konandur. The senior citizens of the
                                    Kondnaur village,Thirthahalli Taluk Shivamogga District, started Vidya
                                    Vardhaka Sangha in 1960 with the objective of starting a High School at Konandur</p>
                                <div id="ourCollege" style="display: none;">
                                    <p style="text-align: justify">In those days, secondary education was a dream to the
                                        people of rural areas. Vidya Vardhaka Sangha with the help of N.E.S.
                                        Shivamogga started National High School at Konandur in 1960.
                                        Sharavathi First Grade College was started in 1983 to provide Higher
                                        Education to the Rural students. Formerly the college was named as
                                        N.E.S. Rural First Grade College of Arts and Commerce. Later the
                                        name was changed as Sharavathi First Grade College, Konandur.
                                        In the beginning the college was affiliated to the University of
                                        Mysore. Now it is affiliated to Kuvempu University. We are proud
                                        to say that the college has got, permanent affiliation and it is
                                        included in 2 (F)and 12 (B) of  U.G.C. act of 1956.
                                        The college is located at Konandur, a small village in Thirthahalli
                                        Taluk, Shivamogga District on Ganga Moola - Tadasa State High way
                                        no # 1. The campus is located in an area of about 35 acres, out of
                                        which the college campus measures about 8.19 acres. It is the only
                                        First Grade College in a radius of about 20 kms which caters the
                                        need of rural students, particularly the women students. The main
                                        objective of the management is to provide quality education to the
                                        rural students. In our college about 5 % of the students are girl
                                        students. They would have been deprived of higher education but for
                                        this attempt of National Education Society ® Shivamogga.
                                        The college has an excellent infrastructure facility, Class rooms,
                                        Assembly hall, Library, Separate rest room for girls and boys,
                                        Ladies hostel, Fully equipped gymnasium and playground.

                                        The college has qualified, hard working enthusiastic faculty.
                                        There is a conscious effort on the part of the teachers continuously
                                        to improve their qualifications and competence as teachers by
                                        participating in research activities, seminars, workshops,
                                        orientation and refresher courses sponsored by U.G.C. and other
                                        bodies. The college has maintained high academic standard and
                                        figures prominently in the list of high achievers.
                                        It has bagged many state and university awards for its extensional
                                        activities. Normally the annual result is above 90 %.
                                        The college provides ample opportunity and support to students
                                        who are good at sports, music, dramatics, elocutions, debating,
                                        N.S.S. activities, etc., and has produced many students who have
                                        excelled in these fields.
                                    </p>
                                </div>
                            <button  onclick="ourCollege()"  id="toggle3" class="btn btn-color" style="background-color: white;color: #2d84d0;">Read More</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseSeven-alt3">
                                    <i class="fa fa-angle-right"></i> Our Vision
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>To make the Institution one of the best places to seek knowledge and wisdom. </li>
                                    <li>To transform the students strength into the strength of  the nation.</li>
                                    <li>To imbibe the culture of patriotism, the virtue of sacrifice, self respect and integrity of character among the students. </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseEight-alt3">
                                    <i class="fa fa-angle-right"></i> Our Mission
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEight-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>To provide higher education even to the girl students and the under privileged class of the rural society.</li>
                                    <li>To become a leading rural institution of par excellence in humanities and commerce.</li>
                                    <li>To prepare the students for global challenges and inter-national outlook.</li>
                                    <li>To make the students highly qualitative to face competitive examinations and sow the seed of research ability among them.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <!-- Panel heading -->
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseOne-alt3">
                                    <i class="fa fa-angle-right"></i> We are awesome Team
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne-alt3" class="panel-collapse collapse">
                            <!-- Panel body -->
                            <div class="panel-body">
                                <p style="text-align: justify;">The college makes sincere efforts to provide good teaching learning experiences to students. It also aims at improving the adequacy and competency of the faculty . The institution facilitates effective teaching, learning programms. A Committee is formed to prepare the academic calendar and the time table. It chalks out all major events and activities in that academic year. So also, the examination Committee monitors the conduct of periodic texts, examinations, performance and progress of the student. Slow learners are advised to attend remedial classes, bridge courses and improvement tests. For advanced learners a study circle is formed and special guidance and books are given.  Computer and  Internet facilities are provided to the  students and faculty.</p>
                                <div id="readMore" style="display: none;">
                                    <p style="text-align: justify">The college has a good faculty who are sincere and committed. Most  of the faculty members have completed M.Phil.  Some of them are doing Ph.D.,  Almost all of them have been participating in various seminars and workshops to keep themselves abreast of the recent developments in their respective disciplines. Apart from regular class room teaching they employ many learner centered methods like group discussions, seminars, assignments and project works. The Teachers also make use of OHP’s, maps, charts, the globe,  Dvd and other audio- visual equipments to make their teaching effective and attractive. Our students secure distinctions and ranks in the University Examinations.  So far the institution has secured  13 ranks.
                                        The Government has stopped filling up newly created vacancies but the management is making temporary appointments. So the teaching schedule of the college is not jeopardized.The institution encourages its faculty to take up research activity and to participate in workshop, seminars etc.
                                        Prof. R.M.Jagadish has completed his research work under F.I.P  and has written his  thesis  to be  submitted for evaluation.  Prof. K.T. Parvathamma  is  also  doing  Ph.D., Eight  lecturers including two guest lecturers  have completed M.Phil and  Two lecturers  have appeared for  M.Phil degree examination.
                                        We have successfully conducted  National level programmes such as  National Integration Camp and  National Adventure programme  under the  N.S.S. banner. It is a land  mark as an extension activity.
                                        Our N.S.S. unit has made a name not only in the Kuvempu University but also  in the state. So far, we have secured  State Awards thrice for outstanding achieve-ments. Therefore we strongly believe that we should tell some thing about the same.
                                        The N.S.S. unit was started  in our college in the year  1989. Our college  and our village  is  the suitable  place  for  the implementation of  Gandhiji's  concept  "Grama Swaraj".
                                        Our  N.S.S. Unit  consist of  students, who are from rural areas. Making them to understand the social values, responsibilities and community services is our objective. Our  N.S.S. volunteers are continuously  participating in programmes such as Pulse-polio campaign, Sapling plants, Literacy Programme,  Drug awareness, AIDS awareness,  Anti Plastic awareness Jatha, blood donation camps etc.
                                    </p>
                                </div>
                                <button  onclick="readMore()"  id="toggle1" class="btn btn-color" style="background-color: white;color: #2d84d0;">Read More</button>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseTwo-alt3">
                                    <i class="fa fa-angle-right"></i>Organization and Management
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                Our management National Education Society ® Shivamogga, Karnataka State. is one of the reputed educational institutions in the state of Karnataka. The founders of the institution were freedom fighters and they established the institution with a foresight and vision. The present management is committed to give quality education to its stakeholders. The management has given the institution very good infrastructure. It has appointed a public relation officer who looks after day to day’s affairs.
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseThree-alt3">
                                    <i class="fa fa-angle-right"></i> Our Objectives
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    <li>To impart quality education to the rural youth.</li>
                                    <li>To facilitate rural woman to higher education.</li>
                                    <li>To Develop all-round personality of the students.</li>
                                    <li>To encourage the students to cultivate  discipline and sense of patriotism</li>
                                    <li>To motivate the potential rural youth to respond to the social problems.</li>
                                    <li>To give computer education.</li>
                                    <li>To inspire the students to carry on our cultural heritage through participation in intellectual, cultural and sports activities.</li>
                                    <li>To assist, socially and economically disadvantaged students to come up in life through education.</li>
                                    <li>To empower the students to became global teachers and in turn to create competence for nation building.</li>
                                    <li>To prepare students to became holistic persons of the global society.</li>
                                    <li>Transforming the hidden potentialities of the students into realities</li>
                                    <li>To chalk our programmes to enhance analytical skills, ICT Skills and communicative skills of students so that they can became globally competent.</li>
                                    <li>Creating Awareness on Health. We conduct Yoga Classes for good health and sound mind.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="font-weight: bold; font-size: 18px">
                                <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseFour-alt3">
                                    <i class="fa fa-angle-right"></i> Teaching,Learning and Evaluation
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour-alt3" class="panel-collapse collapse">
                            <div class="panel-body">
                                The institution has a transparent admission process. The college makes sincere efforts to provide good teaching learning experiences to students. It also aims at improving the adequacy and competency of the faculty . The institution facilitates effective teaching, learning programms. A Committee is formed to prepare the academic calendar and the time table. It chalks out all major events and activities in that academic year.
                            </div>
                        </div>
                    </div>
                <br>
            </div>
        </div>--}}
    </section>
    <marquee class="li customFooter" direction=”right” ng-init="listAllNews()"><%flashNews%></marquee>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Sharavathi First Grade College-Konandur</strong><br>
                            Thirthahalli Taluk Shivamogga District. Karnataka<br>
                            Pin-577 422.</address>
                        <p>
                            <i class="icon-phone"></i>Ph : 08181 276635<br>
                            <i class="icon-envelope-alt"></i>e-mail : sfgckonandur@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                            <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>

                        </ul>
                    </div>
                </div>
    </footer>
</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../sfgc/js/jquery.js"></script>
<script src="../sfgc/js/jquery.easing.1.3.js"></script>
<script src="../sfgc/js/bootstrap.min.js"></script>
<script src="../sfgc/js/jquery.fancybox.pack.js"></script>
<script src="../sfgc/js/jquery.fancybox-media.js"></script>
<script src="../sfgc/js/portfolio/jquery.quicksand.js"></script>
<script src="../sfgc/js/portfolio/setting.js"></script>
<script src="../sfgc/js/jquery.flexslider.js"></script>
<script src="../sfgc/js/animate.js"></script>
<script src="../sfgc/js/custom.js"></script>
<script>function readMore() {
        var x = document.getElementById("readMore");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        var change = document.getElementById("toggle1");
        if (change.innerHTML == "Read More")
        {
            change.innerHTML = "Hide";
        }
        else {
            change.innerHTML = "Read More";
        }
    }</script>
<script>function ourInstitution() {
        var y = document.getElementById("ourInstitution");
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
        var Ychange = document.getElementById("toggle2");
        if (Ychange.innerHTML == "Read More")
        {
            Ychange.innerHTML = "Hide";
        }
        else {
            Ychange.innerHTML = "Read More";
        }
    }
</script>
<script>function ourCollege() {
        var z = document.getElementById("ourCollege");
        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }
        var Zchange = document.getElementById("toggle3");
        if (Zchange.innerHTML == "Read More")
        {
            Zchange.innerHTML = "Hide";
        }
        else {
            Zchange.innerHTML = "Read More";
        }
    }
</script>
</body>
</html>