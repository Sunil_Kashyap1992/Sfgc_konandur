<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <meta charset="utf-8">
    <title>SFGC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="../sfgc/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sfgc/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../sfgc/css/jcarousel.css" rel="stylesheet" />
    <link href="../sfgc/css/flexslider.css" rel="stylesheet" />
    <link href="../sfgc/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="../sfgc/js/app.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="ng-cloak" ng-app="sfgc" ng-controller="Controller" background="../sfgc/img/background.jpg">
<div id="wrapper" style="background-color: transparent">

    <!-- start header -->
    <header style="background-color: transparent">
        <div class="navbar navbar-default navbar-static-top" style="background-color: transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"  href={{url('/')}}><img src="../sfgc/img/logo.png" alt="logo" style="height: 82px;width: 129px"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('motto')}}">Our Motto</a></li>
                        <li><a href="{{url('images')}}">Gallery</a></li>
                        <li><a href="{{url('document')}}">Documents</a></li>
                        <li><a href="">Quick Links</a>
                            <ul class="dropdown">
                                <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                                <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="pricing.html">Pricing</a></li>
                       <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </header><!-- end header -->
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">We are awesome team</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content" style="background-color: transparent">
        <div class="container">
            <div class="col-md-4">
                <div class="post3">
                    <img src="../sfgc/img/08.jpg" alt="">
                </div>
            </div>
            <div class="about">
                <div class="panel-body">
                    <p style="text-align: justify;">The college makes sincere efforts to provide good teaching learning experiences to students.
                        It also aims at improving the adequacy and competency of the faculty . The institution facilitates effective teaching,
                        learning programms. A Committee is formed to prepare the academic calendar and the time table.
                        It chalks out all major events and activities in that academic year. So also, the examination Committee monitors
                        the conduct of periodic texts, examinations, performance and progress of the student. Slow learners are advised to attend
                        remedial classes, bridge courses and improvement tests. For advanced learners a study circle is formed and special guidance
                        and books are given.
                        Computer and  Internet facilities are provided to the  students and faculty.</p>
                    <div>
                        <p style="text-align: justify">The college has a good faculty who are sincere and committed. Most  of the faculty members have completed M.Phil.  Some of them are doing Ph.D.,  Almost all of them have been participating in various seminars and workshops to keep themselves abreast of the recent developments in their respective disciplines. Apart from regular class room teaching they employ many learner centered methods like group discussions, seminars, assignments and project works. The Teachers also make use of OHP’s, maps, charts, the globe,  Dvd and other audio- visual equipments to make their teaching effective and attractive. Our students secure distinctions and ranks in the University Examinations.  So far the institution has secured  13 ranks.
                            The Government has stopped filling up newly created vacancies but the management is making temporary appointments. So the teaching schedule of the college is not jeopardized.The institution encourages its faculty to take up research activity and to participate in workshop, seminars etc.
                            Prof. R.M.Jagadish has completed his research work under F.I.P  and has written his  thesis  to be  submitted for evaluation.  Prof. K.T. Parvathamma  is  also  doing  Ph.D., Eight  lecturers including two guest lecturers  have completed M.Phil and  Two lecturers  have appeared for  M.Phil degree examination.
                            We have successfully conducted  National level programmes such as  National Integration Camp and  National Adventure programme  under the  N.S.S. banner. It is a land  mark as an extension activity.
                            Our N.S.S. unit has made a name not only in the Kuvempu University but also  in the state. So far, we have secured  State Awards thrice for outstanding achieve-ments. Therefore we strongly believe that we should tell some thing about the same.
                            The N.S.S. unit was started  in our college in the year  1989. Our college  and our village  is  the suitable  place  for  the implementation of  Gandhiji's  concept  "Grama Swaraj".
                            Our  N.S.S. Unit  consist of  students, who are from rural areas. Making them to understand the social values, responsibilities and community services is our objective. Our  N.S.S. volunteers are continuously  participating in programmes such as Pulse-polio campaign, Sapling plants, Literacy Programme,  Drug awareness, AIDS awareness,  Anti Plastic awareness Jatha, blood donation camps etc.
                        </p>
                    </div>
                    <!-- Heading -->
                    <div class="block-heading-six">
                        <h4 class="bg-color">Our Team</h4>
                    </div>
                    <br>

                    <!-- Our team starts -->

                    <div class="team-six">
                        <div class="row text-center">
                            <div class="col-md-3 col-sm-6">
                                <!-- Team Member -->
                                <div class="team-member">
                                    <!-- Image -->
                                    <img class="img-responsive" src="../sfgc/img/principal.jpg" alt="">
                                    <!-- Name -->
                                    <h4>Prof. K.M.Sudhakar</h4>
                                    <span class="deg"><h5>Principal</h5></span>
                                    <!-- <p>Perspiciaatis unde omnis iste natus error sit voluptatem accusantium dolore totam rem aperiam.</p> -->
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <!-- Team Member -->
                                <div class="team-member">
                                    <!-- Image -->
                                    <img class="img-responsive" src="../sfgc/img/subhash.jpeg" alt="" style="height: 320px;">
                                    <!-- Name -->
                                    <h4>Prof. M.G.Subhash</h4>
                                    <span class="deg"><h5>Associate Professor and H.O.D.Department of Commerce</h5></span>
                                    <!-- <p>Perspiciaatis unde omnis iste natus error sit voluptatem accusantium dolore totam rem aperiam.</p> -->
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <!-- Team Member -->
                                <div class="team-member">
                                    <!-- Image -->
                                    <img class="img-responsive" src="../sfgc/img/shankar.jpg" alt="" style="height: 320px";>
                                    <!-- Name -->
                                    <h4>Prof. P.M.N.Shankar</h4>
                                    <span class="deg"><h5>Associate Professor and H.O.D.Department of Pol.Science</h5></span>
                                    <!-- <p>Perspiciaatis unde omnis iste natus error sit voluptatem accusantium dolore totam rem aperiam.</p> -->
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <!-- Team Member -->
                                <div class="team-member">
                                    <!-- Image -->
                                    <img class="img-responsive" src="../sfgc/img/physical.jpg" alt="" style="height: 320px;">
                                    <!-- Name -->
                                    <h4>Prof. Gireesha G K</h4>
                                    <span class="deg"><h5>Physical Director</h5></span>
                                    <!-- <p>Perspiciaatis unde omnis iste natus error sit voluptatem accusantium dolore totam rem aperiam.</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Our team ends -->
                </div>
            </div>
        </div>
    </section>
    <marquee class="li customFooter" direction=”right” ng-init="listAllNews()"><%flashNews%></marquee>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Sharavathi First Grade College-Konandur</strong><br>
                            Thirthahalli Taluk Shivamogga District. Karnataka<br>
                            Pin-577 422.</address>
                        <p>
                            <i class="icon-phone"></i>Ph : 08181 276635<br>
                            <i class="icon-envelope-alt"></i>e-mail : sfgckonandur@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <!-- <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                            <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>

                        </ul>
                    </div>
                </div>
    </footer>
</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../sfgc/js/jquery.js"></script>
<script src="../sfgc/js/jquery.easing.1.3.js"></script>
<script src="../sfgc/js/bootstrap.min.js"></script>
<script src="../sfgc/js/jquery.fancybox.pack.js"></script>
<script src="../sfgc/js/jquery.fancybox-media.js"></script>
<script src="../sfgc/js/portfolio/jquery.quicksand.js"></script>
<script src="../sfgc/js/portfolio/setting.js"></script>
<script src="../sfgc/js/jquery.flexslider.js"></script>
<script src="../sfgc/js/animate.js"></script>
<script src="../sfgc/js/custom.js"></script>
<script>function readMore() {
        var x = document.getElementById("readMore");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        var change = document.getElementById("toggle1");
        if (change.innerHTML == "Read More")
        {
            change.innerHTML = "Hide";
        }
        else {
            change.innerHTML = "Read More";
        }
    }</script>
<script>function ourInstitution() {
        var y = document.getElementById("ourInstitution");
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
        var Ychange = document.getElementById("toggle2");
        if (Ychange.innerHTML == "Read More")
        {
            Ychange.innerHTML = "Hide";
        }
        else {
            Ychange.innerHTML = "Read More";
        }
    }
</script>
<script>function ourCollege() {
        var z = document.getElementById("ourCollege");
        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }
        var Zchange = document.getElementById("toggle3");
        if (Zchange.innerHTML == "Read More")
        {
            Zchange.innerHTML = "Hide";
        }
        else {
            Zchange.innerHTML = "Read More";
        }
    }
</script>
</body>
</html>