
<!DOCTYPE html>
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <title>Login</title>
    @include('header')
</head>
<body class="login-content" ng-app="sfgc" ng-controller="LoginController">
<div class="lc-block toggled" id="l-login">
    <img src="../sfgc/img/logo.png" style="width: 70px;height: 70px;">
    <h3>SFGC</h3>
    <p>Login to continue</p>
    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
        <div class="fg-line">
            <input type="emailId" class="form-control" placeholder="Username" ng-model="loginData.emailId">
        </div>
    </div>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
        <div class="fg-line">
            <input type="password" class="form-control" placeholder="Password" ng-model="loginData.password">
        </div>
    </div>

    <div class="clearfix"></div>
    <a href="" class="btn btn-login btn-danger btn-float" ng-click="doLogin(loginData)"><i class="zmdi zmdi-arrow-forward"></i></a>
</div>
@include('scripts');
</body>
</html>