<div class="sidebar-inner c-overflow" ng-controller="SidemenuController">
    <div class="profile-menu">
        <a href="">
            <div class="profile-pic">
                <img src="img/profile-pics/2.jpg" alt="">
            </div>

            <div class="profile-info">
                Admin
                <i class="zmdi zmdi-arrow-drop-down"></i>
            </div>
        </a>

        <ul class="main-menu" style="display:none">
            <li>
                <a href="profile-about.html"><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>

        </ul>
    </div>

    <ul class="main-menu">
        <li><a href="{{url('adminDashboard')}}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
        <li><a href="{{url('adminGallery')}}"><i class="zmdi zmdi-album"></i> Gallery</a></li>
        <li><a href="{{url('adminDocument')}}"><i class="zmdi zmdi-book"></i> Document</a></li>
        <li><a href="{{url('adminFlashNews')}}"><i class="zmdi zmdi-comment-alert"></i> Flash News</a></li>
        <li>
            <a href="{{url('adminLogin')}}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
        </li>
    </ul>
</div>