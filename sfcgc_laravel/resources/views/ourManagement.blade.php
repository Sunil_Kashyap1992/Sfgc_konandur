<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <meta charset="utf-8">
    <title>SFGC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="../sfgc/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sfgc/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../sfgc/css/jcarousel.css" rel="stylesheet" />
    <link href="../sfgc/css/flexslider.css" rel="stylesheet" />
    <link href="../sfgc/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="../sfgc/js/app.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="ng-cloak" ng-app="sfgc" ng-controller="Controller" background="../sfgc/img/background.jpg">
<div id="wrapper" style="background-color: transparent">

    <!-- start header -->
    <header style="background-color: transparent">
        <div class="navbar navbar-default navbar-static-top" style="background-color: transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"  href={{url('/')}}><img src="../sfgc/img/logo.png" alt="logo" style="height: 82px;width: 129px"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('motto')}}">Our Motto</a></li>
                        <li><a href="{{url('images')}}">Gallery</a></li>
                        <li><a href="{{url('document')}}">Documents</a></li>
                        <li><a href="">Quick Links</a>
                            <ul class="dropdown">
                                <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                                <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="pricing.html">Pricing</a></li>
                       <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </header><!-- end header -->
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">Our Management</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content" style="background-color: transparent">
        <div class="container">
            <div class="col-md-4">
                <div class="post3">
                    <img src="../sfgc/img/09.jpg" alt="">
                </div>
            </div>
            <div class="about">
                <div class="panel-body">
                    <p style="text-align: justify;">Our management National Education Society ® Shivamogga, Karnataka State.
                        is one of the reputed educational institutions in the state of Karnataka. The founders of the institution were freedom
                        fighters and they established the institution with a foresight and vision. The present management is committed to give
                        quality education to its stakeholders. The management has given the institution very good infrastructure.
                        It has appointed a public relation officer who looks after day to day’s affairs.</p>
                    <div>
                        <p style="text-align: justify">Late Mr. H.S.Rudrappa, Ex Minister of Karnataka was the founder president of the society,
                            late Mr.S.V. Krishnamurthy Rao, Ex Deputy Speaker of Loksabha, Late Mr. S.R.Nagappa Shetty, Ex-M.L.A.,
                            were the founder secretaries. Late Mr. Jayathirthachar an eminent educationist and leading advocate,
                            Late Mr. P.Murudappa, Gandian and a leading  advocate, Late Sri Mahishi Narasimma Murthy, an eminent
                            scholar and leading advocate, Late Mr.Basappa Patil, Freedom fighter, and Agriculturist and Late Mr.D.S.Dinakar, freedom fighter served
                            as Presidents after Mr. H.S.Rudrappa. Late Mr. Girimaji N. Rajagopal, freedom fighter served as
                            Secretary after Mr. S.R.Nagappa Shetty. Late Mr. Dodya Naik, Ex-M.L.A. served as Treasurer.
                            It is a matter of great pride for the society to remember the visit of Late Dr. Babu Rajendra Prasad,
                            Former President of India in-connection with the opening ceremony of national boys high school
                            in 1949 and also the visit of Late Sri Lal Bahuddur Shastry, Former Prime Minister of India, in-connection with the
                            foundation layingceremony of Kamala Nehru Memorial National College of Arts and Commerce for Women,
                            Shivamogga on 24th January 1965. [ Contact no: 08182-273469,
                            E mail ID -  nesedusociety@gmail.com , Web site: www.nes_shimoga.org ]
                            National Education Society ® started its educational activities by starting a High School at Shivamogga in 1946.
                            Now it is managing 42 educational institutions of various disciplines including Post Graduate course in Computer Applications, Business
                            Administration, Computer Science, Engineering, I.C.S.C. and C.B.S.C. Residential Schools. Out of which 3-General colleges,
                            5-Professional colleges, 2-Technical colleges and the rest are Pre-university colleges, High schools, Residential schools and Primary schools.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <marquee class="li customFooter" direction=”right” ng-init="listAllNews()"><%flashNews%></marquee>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Sharavathi First Grade College-Konandur</strong><br>
                            Thirthahalli Taluk Shivamogga District. Karnataka<br>
                            Pin-577 422.</address>
                        <p>
                            <i class="icon-phone"></i>Ph : 08181 276635<br>
                            <i class="icon-envelope-alt"></i>e-mail : sfgckonandur@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <!-- <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                            <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>

                        </ul>
                    </div>
                </div>
                <!--  <div class="col-lg-3">
                        <div class="widget">
                        <h5 class="widgetheading">Recent News</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="copyright">
                            <p>
                                <span>&copy; Genius Educational 2015 All right reserved. By </span><a href="http://webthemez.com" target="_blank">WebThemez</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="social-network">
                            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>-->
    </footer>
</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../sfgc/js/jquery.js"></script>
<script src="../sfgc/js/jquery.easing.1.3.js"></script>
<script src="../sfgc/js/bootstrap.min.js"></script>
<script src="../sfgc/js/jquery.fancybox.pack.js"></script>
<script src="../sfgc/js/jquery.fancybox-media.js"></script>
<script src="../sfgc/js/portfolio/jquery.quicksand.js"></script>
<script src="../sfgc/js/portfolio/setting.js"></script>
<script src="../sfgc/js/jquery.flexslider.js"></script>
<script src="../sfgc/js/animate.js"></script>
<script src="../sfgc/js/custom.js"></script>
<script>function readMore() {
        var x = document.getElementById("readMore");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        var change = document.getElementById("toggle1");
        if (change.innerHTML == "Read More")
        {
            change.innerHTML = "Hide";
        }
        else {
            change.innerHTML = "Read More";
        }
    }</script>
<script>function ourInstitution() {
        var y = document.getElementById("ourInstitution");
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
        var Ychange = document.getElementById("toggle2");
        if (Ychange.innerHTML == "Read More")
        {
            Ychange.innerHTML = "Hide";
        }
        else {
            Ychange.innerHTML = "Read More";
        }
    }
</script>
<script>function ourCollege() {
        var z = document.getElementById("ourCollege");
        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }
        var Zchange = document.getElementById("toggle3");
        if (Zchange.innerHTML == "Read More")
        {
            Zchange.innerHTML = "Hide";
        }
        else {
            Zchange.innerHTML = "Read More";
        }
    }
</script>
</body>
</html>