<!DOCTYPE html>
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <title>SFGC</title>
    @include('header')
</head>
<body ng-app="sfgc" class="ng-cloak" ng-controller="NewsController" ng-init="listAllNews(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{url('adminFlashNews')}}">Flash News</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar')
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Flash News</h2>
                <ul class="actions" style="margin-right: 5%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleNewsForm()"><i class="fa fa-plus"></i> Add News</button>
                        </a>
                    </li>
                </ul>
            </div>
            <br/>
            <div class="dash-widgets ng-cloak" id="closeNews">
                <div ng-if="newsPresent == true">
                    <div class="table-responsive" tabindex="1" style="overflow: hidden; outline: none;" ng-if="newsPresent == true">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Flash News</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat = "news in flashNews">
                                <td><%$index+1%></td>
                                <td ng-if="$index == 0" ng-show="currentPageForNews ==1 || currentPageForNews == undefined" class="blink_me"><%news.flashNewsTitle%></td>
                                <td ng-if="$index > 0 || currentPageForNews>1"><%news.flashNewsTitle%></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="text-center ng-cloak" ng-show="showPaginationForNews">
                    <br/>
                    <div class="btn-group">
                        <button ng-disabled="currentPageForNews == 1" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllNews(1,previousPageUrlForNews)"><i class="fa fa-chevron-left"></i></button>
                        <button class="btn btn-primary ng-cloak" max-size="maxSize" boundary-links="true"><%currentPageForNews%>/<%lastPageForNews%></button>
                        <button ng-disabled="currentPageForNews == lastPageForNews" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllNews(1,nextPageUrlForNews)"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </div>
                <div class="text-center ng-cloak" ng-show="showPaginationForNews">
                    <br/>
                </div>
                <div class="card" ng-if="newsPresent == false">
                    <div class="card-header ch-alt text-center">
                        <i class="zmdi zmdi-book-image fa-4x"></i>
                    </div>
                    <div class="card-body card-padding text-center">
                        <h2>No News Present</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="news" style="display: none">
                <div class="card-header">
                    <h2>Add News</h2>
                </div>

                <div class="card-body card-padding">
                    <form name = "addImage">
                        <input type="text" class="form-control custom-input-form" placeholder="News" ng-model="news.flashNewsTitle">
                        <br/>
                        <button class="btn btn-default waves-effect" ng-click="toggleNewsForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="addNews(news)">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
@include('scripts');
</body>
</html>