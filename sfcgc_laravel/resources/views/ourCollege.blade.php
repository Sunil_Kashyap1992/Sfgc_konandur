<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <meta charset="utf-8">
    <title>SFGC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="../sfgc/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sfgc/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../sfgc/css/jcarousel.css" rel="stylesheet" />
    <link href="../sfgc/css/flexslider.css" rel="stylesheet" />
    <link href="../sfgc/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="../sfgc/js/app.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="ng-cloak" ng-app="sfgc" ng-controller="Controller" background="../sfgc/img/background.jpg">
<div id="wrapper" style="background-color: transparent">

    <!-- start header -->
    <header style="background-color: transparent">
        <div class="navbar navbar-default navbar-static-top" style="background-color: transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"  href={{url('/')}}><img src="../sfgc/img/logo.png" alt="logo" style="height: 82px;width: 129px"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="active"><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('motto')}}">Our Motto</a></li>
                        <li><a href="{{url('images')}}">Gallery</a></li>
                        <li><a href="{{url('document')}}">Documents</a></li>
                        <li><a href="">Quick Links</a>
                            <ul class="dropdown">
                                <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                                <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="pricing.html">Pricing</a></li>
                       <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </header><!-- end header -->
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">Our College</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content" style="background-color: transparent">
        <div class="container">
            <div class="col-md-4">
                <div class="post3">
                    <img src="../sfgc/img/institution.jpg" alt="">
                </div>
            </div>
            <div class="about">
                <div class="panel-body">
                    <p style="text-align: justify;">National Education Society ® Shivamogga is the parent body of
                        Sharavathi First Grade College, Konandur. The senior citizens of the
                        Kondnaur village,Thirthahalli Taluk Shivamogga District, started Vidya
                        Vardhaka Sangha in 1960 with the objective of starting a High School at Konandur</p>
                    <div>
                        <p style="text-align: justify">In those days, secondary education was a dream to the
                            people of rural areas. Vidya Vardhaka Sangha with the help of N.E.S.
                            Shivamogga started National High School at Konandur in 1960.
                            Sharavathi First Grade College was started in 1983 to provide Higher
                            Education to the Rural students. Formerly the college was named as
                            N.E.S. Rural First Grade College of Arts and Commerce. Later the
                            name was changed as Sharavathi First Grade College, Konandur.
                            In the beginning the college was affiliated to the University of
                            Mysore. Now it is affiliated to Kuvempu University. We are proud
                            to say that the college has got, permanent affiliation and it is
                            included in 2 (F)and 12 (B) of  U.G.C. act of 1956.
                            The college is located at Konandur, a small village in Thirthahalli
                            Taluk, Shivamogga District on Ganga Moola - Tadasa State High way
                            no # 1. The campus is located in an area of about 35 acres, out of
                            which the college campus measures about 8.19 acres. It is the only
                            First Grade College in a radius of about 20 kms which caters the
                            need of rural students, particularly the women students. The main
                            objective of the management is to provide quality education to the
                            rural students. In our college about 5 % of the students are girl
                            students. They would have been deprived of higher education but for
                            this attempt of National Education Society ® Shivamogga.
                            The college has an excellent infrastructure facility, Class rooms,
                            Assembly hall, Library, Separate rest room for girls and boys,
                            Ladies hostel, Fully equipped gymnasium and playground.

                            The college has qualified, hard working enthusiastic faculty.
                            There is a conscious effort on the part of the teachers continuously
                            to improve their qualifications and competence as teachers by
                            participating in research activities, seminars, workshops,
                            orientation and refresher courses sponsored by U.G.C. and other
                            bodies. The college has maintained high academic standard and
                            figures prominently in the list of high achievers.
                            It has bagged many state and university awards for its extensional
                            activities. Normally the annual result is above 90 %.
                            The college provides ample opportunity and support to students
                            who are good at sports, music, dramatics, elocutions, debating,
                            N.S.S. activities, etc., and has produced many students who have
                            excelled in these fields.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <marquee class="li customFooter" direction=”right” ng-init="listAllNews()"><%flashNews%></marquee>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Sharavathi First Grade College-Konandur</strong><br>
                            Thirthahalli Taluk Shivamogga District. Karnataka<br>
                            Pin-577 422.</address>
                        <p>
                            <i class="icon-phone"></i>Ph : 08181 276635<br>
                            <i class="icon-envelope-alt"></i>e-mail : sfgckonandur@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <!-- <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                            <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>

                        </ul>
                    </div>
                </div>
                <!--  <div class="col-lg-3">
                        <div class="widget">
                        <h5 class="widgetheading">Recent News</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="copyright">
                            <p>
                                <span>&copy; Genius Educational 2015 All right reserved. By </span><a href="http://webthemez.com" target="_blank">WebThemez</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="social-network">
                            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>-->
    </footer>
</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../sfgc/js/jquery.js"></script>
<script src="../sfgc/js/jquery.easing.1.3.js"></script>
<script src="../sfgc/js/bootstrap.min.js"></script>
<script src="../sfgc/js/jquery.fancybox.pack.js"></script>
<script src="../sfgc/js/jquery.fancybox-media.js"></script>
<script src="../sfgc/js/portfolio/jquery.quicksand.js"></script>
<script src="../sfgc/js/portfolio/setting.js"></script>
<script src="../sfgc/js/jquery.flexslider.js"></script>
<script src="../sfgc/js/animate.js"></script>
<script src="../sfgc/js/custom.js"></script>
<script>function readMore() {
        var x = document.getElementById("readMore");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        var change = document.getElementById("toggle1");
        if (change.innerHTML == "Read More")
        {
            change.innerHTML = "Hide";
        }
        else {
            change.innerHTML = "Read More";
        }
    }</script>
<script>function ourInstitution() {
        var y = document.getElementById("ourInstitution");
        if (y.style.display === "none") {
            y.style.display = "block";
        } else {
            y.style.display = "none";
        }
        var Ychange = document.getElementById("toggle2");
        if (Ychange.innerHTML == "Read More")
        {
            Ychange.innerHTML = "Hide";
        }
        else {
            Ychange.innerHTML = "Read More";
        }
    }
</script>
<script>function ourCollege() {
        var z = document.getElementById("ourCollege");
        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }
        var Zchange = document.getElementById("toggle3");
        if (Zchange.innerHTML == "Read More")
        {
            Zchange.innerHTML = "Hide";
        }
        else {
            Zchange.innerHTML = "Read More";
        }
    }
</script>
</body>
</html>