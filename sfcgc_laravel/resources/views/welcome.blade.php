<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>SFGC</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="http://webthemez.com" />
    <!-- css -->
    <link href="../sfgc/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../sfgc/css/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../sfgc/css/jcarousel.css" rel="stylesheet" />
    <link href="../sfgc/css/flexslider.css" rel="stylesheet" />
    <link href="../sfgc/js/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="../sfgc/css/style.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    <script src="../sfgc/js/app.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body ng-app="sfgc" ng-controller="Controller"  background="../sfgc/img/background.jpg" style="padding: 15px;">
<div id="wrapper" class="home-page">
    <!-- start header -->
    <header style="background-color: transparent">
        <div class="navbar navbar-default navbar-static-top" style="background-color: transparent">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href={{url('/')}}><img src="../sfgc/img/logo.png" alt="logo" style="height: 82px;width: 129px"/></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{{url('welcome')}}">Home</a></li>
                        <li><a href="{{url('about')}}">About Us</a></li>
                        <li><a href="{{url('motto')}}">Our Motto</a></li>
                        <li><a href="{{url('images')}}">Gallery</a></li>
                        <li><a href="{{url('document')}}">Documents</a></li>
                        <li><a href="">Quick Links</a>
                            <ul class="dropdown">
                                <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                                <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>
                            </ul>
                        </li>
                        <!-- <li><a href="contact.html">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->
    <section id="banner" style="background-color: transparent">

        <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                <li>
                    <img class="responsiveImg" style="height: 800px;" src="../sfgc/img/college.jpg" alt="">
                    <div class="flex-caption container">
                        <h3 style="text-align: center;">ಶರಾವತಿ ಪ್ರಥಮ ದರ್ಜೆ ಕಾಲೇಜು-ಕೋಣಂದೂರು</h3>
                    </div>
                </li>
                <li>
                    <img  class="responsiveImg" style="height: 800px;"  src="../sfgc/img/10.jpg" alt="" >
                    <div class="flex-caption container">
                        <h3 style="text-align: center;">ಶರಾವತಿ ಪ್ರಥಮ ದರ್ಜೆ ಕಾಲೇಜು-ಕೋಣಂದೂರು</h3>
                    </div>
                </li>
                <li>
                    <img  class="responsiveImg" style="height: 800px;"  src="../sfgc/img/05.jpg" alt="" >
                    <div class="flex-caption container">
                        <h3 style="text-align: center;">ಶರಾವತಿ ಪ್ರಥಮ ದರ್ಜೆ ಕಾಲೇಜು-ಕೋಣಂದೂರು</h3>
                    </div>
                </li>
                <li>
                    <img  class="responsiveImg" style="height: 800px;" src="../sfgc/img/09.jpg" alt="" >
                    <div class="flex-caption container">
                        <h3 style="text-align: center;">ಶರಾವತಿ ಪ್ರಥಮ ದರ್ಜೆ ಕಾಲೇಜು-ಕೋಣಂದೂರು</h3>
                    </div>
                </li>
            </ul>
        </div>
        <marquee class="li customFooter" direction=”right” ng-init="listAllNews()"><%flashNews%></marquee>
        <!-- end slider -->

    </section>
    <section id='events'>
        <div>
            <div class="row">
                <div class="col-md-12">
                    <div class="aligncenter"><h2 class="aligncenter">Message From Principal</h2></div>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" style="margin-top:3%">
                    <div class="post3">
                        <img src="../sfgc/img/principal_home.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="aligncenter" style="text-align: justify;">
                        <p style="text-align: justify;">
                            National Education Society ® Shivamogga is the parent body of Sharavathi First Grade College, Konandur.
                            The senior citizens of the Kondnaur village, Thirthahalli Taluk Shivamogga District, started Vidya Vardhaka Sangha in 1960 with the objective
                            of starting a High School at Konandur. In those days, secondary education was a dream to the people of rural areas. Vidya Vardhaka Sangha with
                            the help of N.E.S. Shivamogga started National High School at Konandur in 1960. Sharavathi First Grade College was started in 1983 to provide Higher
                            Education to the rural students.</p>
                        <div id="principalMessage" style="display: none;">
                            Formerly the college was named as N.E.S. Rural First Grade College of Arts and Commerce. Later the name was changed
                            as Sharavathi First Grade College, Konandur. In the beginning the college was affiliated to the University of Mysore. Now it is affiliated to
                            Kevempu University. We are proud to say that the college has got, permanent affiliation. The college is located at Konandur, a small village
                            in Thirthahalli Taluk, Shivamogga District on Ganga Moola - Tadasa State High way no # 1. The campus is located in an area of about 35 acres,
                            out of which the college campus measures about 8 acres. It is the only First Grade College in a radius of about 20 kms which caters the need of
                            rural students, particularly the women students. The main objective of the management is to provide quality education to the rural students.
                            In our college about 65 % of the students are girl students. They would have been deprived of higher education but for this attempt of National
                            Education Society ® Shivamogga. The college has an excellent infrastructure facility :- Class rooms , Assembly hall, Library , Separate rest room
                            for girls and boys, Ladies hostel, Fully equipped gymnasium and playground . The college has qualified, hard working enthusiastic faculty.
                            There is a conscious effort on the part of the teachers continuously to improve their qualifications and competence as teachers by participating
                            in research activities, seminars, workshops, orientation and refresher courses sponsored by U.G.C. and other bodies. The college has maintained high
                            academic standard and figures prominently in the list of high achievers. It has bagged many state and university awards for its extensional activities.
                            Normally the annual result is above 95 %. The college provides ample opportunity and support to students who are good at sports, music, dramatics,
                            elocutions,
                            debating, N.S.S. activities, etcetera and has produced many students who have excelled in these fields.
                        </div>
                        <button  onclick="principalMessage()"  id="toggle3" class="btn btn-color" style="background-color: #2d84d0;color: white;">Read More</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id='events'>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="aligncenter"><h2 class="aligncenter">Our Events</h2>The institution believes in the overall development of the personality of student. Our aim is to impart value added quality education. Specific goals and objectives are set and all employees are impressed upon realizing the goal.</div>
                    <br>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="post3">
                        <img src="../sfgc/img/05.jpg" alt="">
                        <!-- <a href="#">
                            <time datetime="2015-03-01">
                                <span class="year">2017</span>
                                <span class="month">Feb</span>
                            </time>
                            <p>Lorem ipsum dolor sit amet, consectetur adipis.</p>
                        </a> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="post3">
                        <img src="../sfgc/img/06.jpg"alt="">
                        <!-- <a href="#">
                            <time datetime="2015-03-01">
                                <span class="year">2017</span>
                                <span class="month">March</span>
                            </time>
                            <p>Apsum dolor sit amet, consectetur adipisdslif.</p>
                        </a> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="post3">
                        <img  src="../sfgc/img/07.jpg" alt="">
                        <!--  <a href="#">
                             <time datetime="2015-03-01">
                                 <span class="year">2017</span>
                                 <span class="month">April</span>
                             </time>
                             <p>Dolor sit amet, consectetur adipisic indfeft</p>
                         </a> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
<section>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="aligncenter" style="text-align: justify;"><h2 class="aligncenter">Our Students</h2>We  are  very proud to say that in the span of 25 years our institution has made fame and name in the university as a model rural college.  We have enabled hundreds of  under privilege rural youths to graduate and get employment.  We have  successfully fulfilled our  moto: to make the  institution one of the best places for the seek of knowledge and wisdom, to provide higher education to the girl student. A host of academic and co-curricular activities take place every year, supplementing the class room work.  Student representation is provided in various committees where they are free to express their opinion and  grievances.  Rather than having an exclusively exam oriented approach the college  practices comprehensive strategies  to promote advancement in essential  areas. .</div>
            <br/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <img src="../sfgc/img/04.jpg" alt="">
            <div class="space"></div>
        </div>
        <div class="col-md-6" style="text-align: justify;">
            <p><i class="fa fa-arrow-circle-right pr-10 colored"></i> In the class room, debates, group discussions, seminars and internal assessment tests are arranged to help  students in their intellectual development. This helps them to shed their inhibitions and boost their confidence. The cultural and sports committee searches for talents among students and they participate in many such competitions and bring honour to the college.</p>
            <p><i class="fa fa-arrow-circle-right pr-10 colored"></i> Guest lectures, visits of eminent personalities and great achievers motivate our students. We have been arranging number of such programs to inspire our students.</p>
            <p><i class="fa fa-arrow-circle-right pr-10 colored"></i> Prizes and awards are given every year on the cultural day to encourage meritorious and talented students. Many of our Committees and cells have been helping students through Students Counseling Cell, Placement Cell, Mid-day meal committee and student welfare committee.  We have also arranged campus selection by reputed companies</p>
            <!-- <ul class="list-unstyled">
                <li><i class="fa fa-arrow-circle-right pr-10 colored"></i> Lorem ipsum enimdolor sit amet</li>
                <li><i class="fa fa-arrow-circle-right pr-10 colored"></i> Explicabo deleniti neque aliquid</li>
                <li><i class="fa fa-arrow-circle-right pr-10 colored"></i> Consectetur adipisicing elit</li>
                <li><i class="fa fa-arrow-circle-right pr-10 colored"></i> Lorem ipsum dolor sit amet</li>
                <li><i class="fa fa-arrow-circle-right pr-10 colored"></i> Quo issimos molest quibusdam temporibus</li>
            </ul> -->
        </div>
    </div>

</div>
</section>
<!--
    <section id="clients">
        <div class="container">
                <div class="row">
            <div class="col-md-12">
                <div class="aligncenter"><h2 class="aligncenter">Campus Placement</h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus ovident, doloribus omnis minus temporibus perferendis nesciunt..</div>
                <br/>
            </div>
        </div>
            <div class="row">
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client1"></div>
                </div>
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client2"></div>
                </div>
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client3"></div>
                </div>
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client1"></div>
                </div>
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client2"></div>
                </div>
                <div class="col-md-2 col-sm-4 client">
                    <div class="img client3"></div>
                </div>
            </div>
        </div>
    </section> -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Our Contact</h5>
                        <address>
                            <strong>Sharavathi First Grade College-Konandur</strong><br>
                            Thirthahalli Taluk Shivamogga District. Karnataka<br>
                            Pin-577 422.</address>
                        <p>
                            <i class="icon-phone"></i>Ph : 08181 276635<br>
                            <i class="icon-envelope-alt"></i>e-mail : sfgckonandur@gmail.com
                        </p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <!-- <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="#">Latest Events</a></li>
                            <li><a href="#">Terms and conditions</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Career</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-lg-3">
                    <div class="widget">
                        <h5 class="widgetheading">Quick Links</h5>
                        <ul class="link-list">
                            <li><a href="http://logisys.net.in/studentportal/sign-in.html">Student Portal Link</a></li>
                            <li><a href="http://logisys.net.in/results/kus/">Result Link</a></li>

                        </ul>
                    </div>
                </div>
                <!--  <div class="col-lg-3">
                        <div class="widget">
                        <h5 class="widgetheading">Recent News</h5>
                        <ul class="link-list">
                            <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
                            <li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
                            <li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="copyright">
                            <p>
                                <span>&copy; Genius Educational 2015 All right reserved. By </span><a href="http://webthemez.com" target="_blank">WebThemez</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <ul class="social-network">
                            <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>-->
    </footer>
</div>
<!-- <a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a> -->
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../sfgc/js/jquery.js"></script>
<script src="../sfgc/js/jquery.easing.1.3.js"></script>
<script src="../sfgc/js/bootstrap.min.js"></script>
<script src="../sfgc/js/jquery.fancybox.pack.js"></script>
<script src="../sfgc/js/jquery.fancybox-media.js"></script>
<script src="../sfgc/js/portfolio/jquery.quicksand.js"></script>
<script src="../sfgc/js/portfolio/setting.js"></script>
<script src="../sfgc/js/jquery.flexslider.js"></script>
<script src="../sfgc/js/animate.js"></script>
<script src="../sfgc/js/custom.js"></script>
<script src="../sfgc/js/owl-carousel/owl.carousel.js"></script>
<script>function principalMessage() {
        var z = document.getElementById("principalMessage");
        if (z.style.display === "none") {
            z.style.display = "block";
        } else {
            z.style.display = "none";
        }
        var Zchange = document.getElementById("toggle3");
        if (Zchange.innerHTML == "Read More")
        {
            Zchange.innerHTML = "Hide";
        }
        else {
            Zchange.innerHTML = "Read More";
        }
    }
</script>
</body>
</html>
