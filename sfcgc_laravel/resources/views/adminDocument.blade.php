<!DOCTYPE html>
<head>
    <link rel="shortcut icon" href="../sfgc/img/logo.png">
    <title>SFGC</title>
    @include('header')
</head>
<body ng-app="sfgc" class="ng-cloak" ng-controller="DocumentsController" ng-init="listAllDocuments(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{url('adminDocument')}}">Documents</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar')
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Documents</h2>
                <ul class="actions" style="margin-right: 5%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleDocumentForm()"><i class="fa fa-plus"></i> Add Doc</button>
                        </a>
                    </li>
                </ul>
            </div>
            <br/>
            <div class="dash-widgets ng-cloak" id="closeDocument">
                <div ng-if="documentsPresent == true">
                    <div class="row media ng-cloak" >
                        <div class="col-sm-2 ng-cloak" ng-repeat="document in documents" style="background-color: white;margin-bottom: 10px;width: 40%">
                            <iframe ng-src="<%document.documentPath%>" frameborder="0" style="height: 300px;width: 98%"></iframe>
                            <p class="text-center" style="color: #0d8aee"><b><%document.documentTitle%></b>&nbsp;&nbsp;<button class="btn btn-info btn-icon-text waves-effect waves-effect" style="float: right;margin-right: 10px" ng-click="deleteDocument(document)"><i class="fa fa-trash"></i> </button></p>
                        </div>
                    </div>
                </div>
                <div class="text-center ng-cloak" ng-show="showPaginationForDocument">
                    <br/>
                    <div class="btn-group">
                        <button ng-disabled="currentPageForDocument == 1" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllDocuments(1,previousPageUrlForDocument)"><i class="fa fa-chevron-left"></i></button>
                        <button class="btn btn-primary ng-cloak" max-size="maxSize" boundary-links="true"><%currentPageForDocument%>/<%lastPageForDocument%></button>
                        <button ng-disabled="currentPageForDocument == lastPageForDocument" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllDocuments(1,nextPageUrlForDocument)"><i class="fa fa-chevron-right"></i></button>
                    </div>
                </div>
                <div class="text-center ng-cloak" ng-show="showPaginationForDocument">
                    <br/>
                </div>
                <div class="card" ng-if="documentsPresent == false">
                    <div class="card-header ch-alt text-center">
                        <i class="zmdi zmdi-book-image fa-4x"></i>
                    </div>
                    <div class="card-body card-padding text-center">
                        <h2>No Documents Present</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="document" style="display: none">
                <div class="card-header">
                    <h2>Add Document</h2>
                </div>

                <div class="card-body card-padding">
                    <form name = "addDocument">
                        <input type="text" class="form-control custom-input-form" placeholder="Document Name" ng-model="document.documentTitle" name="documentTitle" id="documentTitle" >
                        <br/>
                        <input type="file" class="form-control custom-input-form" placeholder="document" file-model="document.documentFile" name="documentFile" id="documentFile" value="documentFile" >
                        <br/>
                        <button class="btn btn-default waves-effect" ng-click="toggleDocumentForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="addDocumentFunction(document)">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<div class="modal fade" id="documentModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="viewModalLabel">View Document</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <iframe ng-src=<%documentPath%> style="height: 400px;width: 500px;"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@include('scripts');
</body>
</html>