var app = angular.module('sfgc', [])
var LIST_GALLERY_URL = "gallery/listAll";
var LIST_GALLERY_SUCCESS_CODE = 7000;
//var BASE_URL = "http://sfgckonandur.in/";
var BASE_URL = "http://localhost:8000/";
var HTTP_STATUS_CODE = 200;
var LIST_NEWS_SUCCESS_CODE = 5000;
var LIST_NEWS_URL = "flashNews/listAll";
var SUCCESS_STATUS_TEXT = "SUCCESS";
var LIST_DOCUMENTS_SUCCESS_CODE = 3000;
var LIST_DOCUMENTS_URL = "document/listAll";
app
    .config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    })
app
.controller('Controller',function($scope,$http,$sce){
	$scope.listAllImages =function(paginateFlag,url){
        if(paginateFlag == 0){
            $scope.galleryListUrl = BASE_URL+LIST_GALLERY_URL;
        }
        else{
            $scope.galleryListUrl = url;
        }
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.galleryListUrl,
			params : {
				filterType : 1
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_GALLERY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.lastPageForGallery = response.data.data.lastPage;
                    $scope.imagesPresent =true;
                    $scope.showPaginationForGallery = true;
                    if($scope.lastPageForGallery == "NULL"){
                        $scope.showPaginationForGallery = false;
                        $scope.images=response.data.data.data;
                    }
                    else{
                        $scope.images=response.data.data.data;
                    }
                    $scope.previousPageUrlForGallery = response.data.data.previousPageUrl;
                    $scope.nextPageUrlForGallery = response.data.data.nextPageUrl;
                    $scope.currentPageForGallery = response.data.data.currentPage;
				}
				else{
					console.log("no data found");
                    $scope.imagesPresent =false;
				}
			}
			else{
				console.log("no data found");
                $scope.imagesPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.imagesPresent =false;
		});
	}


	$scope.listAllNews =function(){
        $scope.newsListUrl = BASE_URL+LIST_NEWS_URL;
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.newsListUrl,
			params : {
				filterType : 1
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_NEWS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.flashNews=response.data.data;
				}
				else{
					console.log("no data found");
                    $scope.newsPresent =false;
				}
			}
			else{
				console.log("no data found");
                $scope.newsPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.newsPresent =false;
		});
	}

	$scope.listAllDocuments =function(paginateFlag,url){
        if(paginateFlag == 0){
            $scope.documentListUrl = BASE_URL+LIST_DOCUMENTS_URL;
        }
        else{
            $scope.documentListUrl = url;
        }
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.documentListUrl,
			params : {
				filterType : 1
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_DOCUMENTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.lastPageForDocument = response.data.data.lastPage;
                    $scope.documentsPresent =true;
                    $scope.showPaginationForDocument = true;
                    if($scope.lastPageForDocument == "NULL"){
                        $scope.showPaginationForDocument = false;
                        $scope.documents=response.data.data.data;
                    }
                    else{
                        $scope.documents=response.data.data.data;
                    }
                  	for(i=0;i<$scope.documents.length;i++){
                  		$scope.documents[i].documentPath = $sce.trustAsResourceUrl(String($scope.documents[i].documentPath));
                  	}
                    $scope.previousPageUrlForDocument = response.data.data.previousPageUrl;
                    $scope.nextPageUrlForDocument = response.data.data.nextPageUrl;
                    $scope.currentPageForDocument = response.data.data.currentPage;
				}
				else{
					console.log("no data found");
                    $scope.documentsPresent =false;
				}
			}
			else{
				console.log("no data found");
                $scope.documentsPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.documentsPresent =false;
		});
	}

})