app
.controller('LoginController',function($scope,$http){
	localStorage.clear();
	$scope.doLogin = function(loginData){
		$scope.ajaxPromise = $http({
			method : "POST",
			url : BASE_URL+LOGIN_URL,
			params : {
				email : loginData.emailId,
				password : loginData.password,
				typeOfUser : 0
			}
		}).then(function success(response){
			console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LOGIN_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
 					$scope.loginTime = new Date().getTime();
                	localStorage.setItem("sfgc_loginTime",$scope.loginTime);
                	localStorage.setItem("sfgc_token",response.data.data.token.token);
                	window.location.href = 'adminDashboard'
				}
				else{
					notify('Login Failed', 'danger');
				}
			}
			else{
				notify('Login Failed', 'danger');
			}
		},function failure(){
			notify('Login Failed --> Server Error', 'danger');
		});
	}
})