var app = angular.module('sfgc', [])

app
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
})
.service('TokenVerifierService', ['$http','$rootScope', function ($http,$rootScope) {
    this.verifyToken = function(errorCode,statusText,statusData){
        if(errorCode == TOKEN_EXPIRED_CODE && statusText == FAILURE_STATUS_TEXT && statusData == TOKEN_EXPIRED_TEXT){
            redirectToLogin();
        }
        else if(errorCode == TOKEN_ABSENT_CODE && statusText == FAILURE_STATUS_TEXT && statusData == TOKEN_ABSENT_TEXT){
            redirectToLogin();
        }
        else if(errorCode == INVALID_TOKEN_CODE && statusText == FAILURE_STATUS_TEXT && statusData == INVALID_TOKEN_TEXT){
            redirectToLogin();
        }
        else{
           // viviktaDebugConsole("No data found");
        }  
    }
  
    //function to redirect to login page
    function redirectToLogin(typeOfUser){
        window.location.href="https://sfgckonandur.in/adminLogin";
    }
}])