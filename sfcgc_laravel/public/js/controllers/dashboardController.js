app
.controller('DashboardController',function($scope,$http,TokenVerifierService){
	if(localStorage.getItem("sfgc_token"))
	   $scope.authToken = localStorage.getItem("sfgc_token");
    else
        TokenVerifierService.verifyToken(TOKEN_EXPIRED_CODE,FAILURE_STATUS_TEXT,TOKEN_EXPIRED_TEXT);
	$scope.listDashboard=function(){
		$scope.ajaxPromise = $http({
			method : "GET",
			url : BASE_URL+LIST_DASHBOARD_URL,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_DASHBOARD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					$scope.totalDocumentCount = response.data.data.totalDocumentCount;
					$scope.totalFlashNewsCount = response.data.data.totalFlashNewsCount;
					$scope.totalImageCount = response.data.data.totalImageCount;
				}
				else{
					console.log("no data found");
					$scope.totalDocumentCount = 0;
					$scope.totalFlashNewsCount = 0;
					$scope.totalImageCount = 0;
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				console.log("no data found");
				$scope.totalDocumentCount = 0;
				$scope.totalFlashNewsCount = 0;
				$scope.totalImageCount = 0;
			}
		},function failure(){
			console.log("no data found");
			$scope.totalDocumentCount = 0;
			$scope.totalFlashNewsCount = 0;
			$scope.totalImageCount = 0;
		});
	}
})