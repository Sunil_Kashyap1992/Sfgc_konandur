app
.controller('NewsController',function($scope,$http,TokenVerifierService){
	if(localStorage.getItem("sfgc_token"))
       $scope.authToken = localStorage.getItem("sfgc_token");
    else
        TokenVerifierService.verifyToken(TOKEN_EXPIRED_CODE,FAILURE_STATUS_TEXT,TOKEN_EXPIRED_TEXT);

	$scope.isNewsFormOpened = false;
	$scope.toggleNewsForm = function(){
		if($scope.isNewsFormOpened == false){
			$("#news").slideDown("slow");
			$("#closeNews").slideUp("slow");
			$scope.isNewsFormOpened = true;
		}
		else{
			$("#closeNews").slideDown("slow");
			$("#news").slideUp("slow");
			$scope.isNewsFormOpened = false;
		}
	};	

	$scope.listAllNews =function(paginateFlag,url){
        if(paginateFlag == 0){
            $scope.newsListUrl = BASE_URL+LIST_NEWS_URL;
        }
        else{
            $scope.newsListUrl = url;
        }
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.newsListUrl,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_NEWS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.lastPageForNews = response.data.data.lastPage;
                    $scope.newsPresent =true;
                    $scope.showPaginationForNews = true;
                    if($scope.lastPageForNews == "NULL"){
                        $scope.showPaginationForNews = false;
                        $scope.flashNews=response.data.data.data;
                    }
                    else{
                        $scope.flashNews=response.data.data.data;
                    }
                    $scope.previousPageUrlForNews = response.data.data.previousPageUrl;
                    $scope.nextPageUrlForNews = response.data.data.nextPageUrl;
                    $scope.currentPageForNews = response.data.data.currentPage;
				}
				else{
					console.log("no data found");
                    $scope.newsPresent =false;
                    TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				console.log("no data found");
                $scope.newsPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.newsPresent =false;
		});
	}

    $scope.addNews = function(newsObject){
        $scope.ajaxPromise = $http({
            method : "POST",
            url : BASE_URL+ADD_NEWS_URL,
            params : {
                token : $scope.authToken,
                flashNewsTitle : newsObject.flashNewsTitle
            }
        }).then(function success(response){
            console.log(JSON.stringify(response));
            if(response.status == HTTP_STATUS_CODE){
                if(response.data.errorCode == ADD_NEWS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    notify('Flash News Added Successfully', 'success');
                    $scope.listAllNews(0,'');
                    $scope.toggleNewsForm();
                }
                else{
                    notify('Flash News Cannot be  Added', 'danger');
                    TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
                }
            }
            else{
                notify('Flash News Cannot be  Added', 'danger');
            }
        },function failure(){
            notify('Flash News Cannot be  Added', 'danger');
        });
    };
})