<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlashNews extends Model
{
    protected $table = 'flash_news_table';
}
