<?php
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE');

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/motto', function () {
    return view('motto');
});
Route::get('/images', function () {
    return view('gallery');
});
Route::get('/document', function () {
    return view('document');
});
Route::get('/ourInstitution', function () {
    return view('ourInstitution');
});
Route::get('/ourCollege', function () {
    return view('ourCollege');
});
Route::get('/ourVision', function () {
    return view('ourVision');
});
Route::get('/ourMission', function () {
    return view('ourMission');
});
Route::get('/ourTeam', function () {
    return view('ourTeam');
});
Route::get('/ourManagement', function () {
    return view('ourManagement');
});
Route::get('/ourObjectives', function () {
    return view('ourObjectives');
});
Route::get('/ourTeaching', function () {
    return view('ourTeaching');
});
Route::get('/adminLogin', function () {
    return view('adminLogin');
});
Route::get('/adminDashboard', function () {
    return view('adminDashboard');
});
Route::get('/adminDocument', function () {
    return view('adminDocument');
});
Route::get('/adminGallery', function () {
    return view('adminGallery');
});
Route::get('/adminFlashNews', function () {
    return view('adminFlashNews');
});
Route::group(['prefix' => 'login'],function()
{
    Route::post('authenticate','AuthenticateController@authenticate');

});
Route::group(['prefix' => 'dashboard'],function()
{
    Route::get('getData','DashboardController@getData');

});
Route::group(['prefix' => 'document'],function()
{
    Route::post('add','DocumentController@add');
    Route::get('listAll','DocumentController@listAll');
    Route::delete('delete','DocumentController@delete');

});
Route::group(['prefix' => 'flashNews'],function()
{
    Route::post('add','FlashNewsController@add');
    Route::get('listAll','FlashNewsController@listAll');

});
Route::group(['prefix' => 'gallery'],function()
{
    Route::post('add','GalleryController@add');
    Route::get('listAll','GalleryController@listAll');
    Route::delete('delete','GalleryController@delete');

});