<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class GalleryController extends Controller
{
    public function add(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {

            $path = public_path();

            $generateUniqueId = new GenerateUUID();
            $galleryId = $generateUniqueId->getUniqueId();

            $galleryPath = '';

            if(Input::file('gallery') != "" || Input::file('gallery') != null)
            {
                $fileExtension = pathinfo(Input::file('gallery')->getClientOriginalName(),PATHINFO_EXTENSION);
                Input::file('gallery')->move("$path/sfgc/gallery/", $galleryId.".".$fileExtension);
                $galleryPath = "sfgc/gallery/".$galleryId.".".$fileExtension;
            }

            $addGallery = new Gallery();
            $addGallery->galleryId = $galleryId;
            $addGallery->imagePath = $galleryPath;
            $addGallery->save();

            if(!$addGallery->save())
            {
                $returnValues = new ReturnController("6002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;

            }
            else
            {
                $returnValues = new ReturnController("6000", "SUCCESS", "");
                $return = $returnValues->returnValues();
                return $return;

            }


        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $listGallery = Gallery::get();

        if(count($listGallery) <=0)
        {
            $returnValues = new ReturnController("7002", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $listGalleryFound = [];

            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    if(count($listGallery) <= 8)
                    {
                        foreach ($listGallery as $gallery)
                        {
                            $tempArray = [];
                            $tempArray['galleryId'] = $gallery['galleryId'];
                            $tempArray['imagePath'] = $DEFAULT_PATH.$gallery['imagePath'];
                            array_push($listGalleryFound,$tempArray);
                        }
                        $data = [
                            "lastPage" => "NULL",
                            "data" => $listGalleryFound];

                        $returnValues = new ReturnController("7000", "SUCCESS", $data);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $listGallery = Gallery::paginate(8);

                        foreach ($listGallery as $gallery)
                        {
                            $tempArray = [];
                            $tempArray['galleryId'] = $gallery['galleryId'];
                            $tempArray['imagePath'] = $DEFAULT_PATH.$gallery['imagePath'];
                            array_push($listGalleryFound,$tempArray);
                        }
                        $data=[
                            "total" => $listGallery->total(),
                            "nextPageUrl" => $listGallery->nextPageUrl(),
                            "previousPageUrl" => $listGallery->previousPageUrl(),
                            "currentPage" => $listGallery->currentPage(),
                            "lastPage" => $listGallery->lastPage(),
                            "data" => $listGalleryFound
                        ];
                        $returnValues = new ReturnController("7000","SUCCESS",$data);
                        $return = $returnValues->returnValues();
                        return $return;

                    }
                    break;
                case "1":
                    $listGallery = Gallery::get();
                    if(count($listGallery) <= 16)
                    {
                        foreach ($listGallery as $gallery)
                        {
                            $tempArray = [];
                            $tempArray['galleryId'] = $gallery['galleryId'];
                            $tempArray['imagePath'] = $DEFAULT_PATH.$gallery['imagePath'];
                            array_push($listGalleryFound,$tempArray);
                        }
                        $data = [
                            "lastPage" => "NULL",
                            "data" => $listGalleryFound];

                        $returnValues = new ReturnController("7000", "SUCCESS", $data);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $listGallery = Gallery::paginate(16);

                        foreach ($listGallery as $gallery)
                        {
                            $tempArray = [];
                            $tempArray['galleryId'] = $gallery['galleryId'];
                            $tempArray['imagePath'] = $DEFAULT_PATH.$gallery['imagePath'];
                            array_push($listGalleryFound,$tempArray);
                        }
                        $data=[
                            "total" => $listGallery->total(),
                            "nextPageUrl" => $listGallery->nextPageUrl(),
                            "previousPageUrl" => $listGallery->previousPageUrl(),
                            "currentPage" => $listGallery->currentPage(),
                            "lastPage" => $listGallery->lastPage(),
                            "data" => $listGalleryFound
                        ];
                        $returnValues = new ReturnController("7000","SUCCESS",$data);
                        $return = $returnValues->returnValues();
                        return $return;

                    }
                    break;

            }
        }
    }
    public function delete(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $checkGallery =  Gallery::where('galleryId',$request->input('galleryId'))->first();

            if(count($checkGallery) <=0)
            {
                $returnValues = new ReturnController("10002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteGallery = Gallery::where('galleryId',$request->input('galleryId'))->delete();

                if($deleteGallery < 0)
                {
                    $returnValues = new ReturnController("10002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;

                }
                else
                {
                    $filename =  $checkGallery['imagePath'];
                    $galleryId = $checkGallery['galleryId'];
                    $path = public_path();
                    $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                    unlink($path.'/sfgc/gallery/'.$galleryId.".".$file_ext);
                    $returnValues = new ReturnController("10000", "SUCCESS", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
}
