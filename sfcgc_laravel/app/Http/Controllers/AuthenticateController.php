<?php

namespace App\Http\Controllers;

use App\Login;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function index()
    {
        // TODO: show users
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                $returnValues = new ReturnController("401", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
        } catch (JWTException $e) {
            // something went wrong
            $returnValues = new ReturnController("500", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }

        $listLogin = Login::where('email',$request->input('email'))->first();

        if(count($listLogin) <=0)
        {
            $returnValues = new ReturnController("1002", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $token = compact('token');
            $data = [
                "token"=>$token,
                "name" => $listLogin['name']];

            $returnValues = new ReturnController("1000", "SUCCESS", $data);
            $return = $returnValues->returnValues();
            return $return;
        }
    }
}
