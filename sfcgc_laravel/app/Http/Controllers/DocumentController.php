<?php

namespace App\Http\Controllers;

use App\Document;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class DocumentController extends Controller
{
    public function add(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {

            $path = public_path();

            $generateUniqueId = new GenerateUUID();
            $documentId = $generateUniqueId->getUniqueId();

            $documentPath = '';

            if(Input::file('document') != "" || Input::file('document') != null)
            {
                $fileExtension = pathinfo(Input::file('document')->getClientOriginalName(),PATHINFO_EXTENSION);
                Input::file('document')->move("$path/sfgc/documents/", $documentId.".".$fileExtension);
                $documentPath = "sfgc/documents/".$documentId.".".$fileExtension;
            }

            $addDocument = new Document();
            $addDocument->documentId = $documentId;
            $addDocument->documentTitle = $request->input('documentTitle');
            $addDocument->documentPath = $documentPath;
            $addDocument->save();

            if(!$addDocument->save())
            {
                $returnValues = new ReturnController("2002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("2000", "SUCCESS", "");
                $return = $returnValues->returnValues();
                return $return;

            }


        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $listDocument = Document::orderBy('id','desc')->get();

        if(count($listDocument) <=0)
        {
            $returnValues = new ReturnController("3002", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $listDocumentFound = [];

            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    if(count($listDocument) <= 4)
                    {
                        foreach ($listDocument as $document)
                        {
                            $tempArray = [];
                            $tempArray['documentId'] = $document['documentId'];
                            $tempArray['documentTitle'] = $document['documentTitle'];
                            $tempArray['documentPath'] = $DEFAULT_PATH.$document['documentPath'];
                            array_push($listDocumentFound,$tempArray);
                        }
                        $data = [
                            "lastPage" => "NULL",
                            "data" => $listDocumentFound];

                        $returnValues = new ReturnController("3000", "SUCCESS", $data);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $listDocument = Document::orderBy('id','desc')->paginate(4);

                        foreach ($listDocument as $document)
                        {
                            $tempArray = [];
                            $tempArray['documentId'] = $document['documentId'];
                            $tempArray['documentTitle'] = $document['documentTitle'];
                            $tempArray['documentPath'] = $DEFAULT_PATH.$document['documentPath'];
                            array_push($listDocumentFound,$tempArray);
                        }
                        $data=[
                            "total" => $listDocument->total(),
                            "nextPageUrl" => $listDocument->nextPageUrl(),
                            "previousPageUrl" => $listDocument->previousPageUrl(),
                            "currentPage" => $listDocument->currentPage(),
                            "lastPage" => $listDocument->lastPage(),
                            "data" => $listDocumentFound
                        ];
                        $returnValues = new ReturnController("3000","SUCCESS",$data);
                        $return = $returnValues->returnValues();
                        return $return;

                    }
                break;
                case "1":
                    $listDocument = Document::orderBy('id','desc')->get();
                    if(count($listDocument) <= 15)
                    {
                        foreach ($listDocument as $document)
                        {
                            $tempArray = [];
                            $tempArray['documentId'] = $document['documentId'];
                            $tempArray['documentTitle'] = $document['documentTitle'];
                            $tempArray['documentPath'] = $DEFAULT_PATH.$document['documentPath'];
                            array_push($listDocumentFound,$tempArray);
                        }
                        $data = [
                            "lastPage" => "NULL",
                            "data" => $listDocumentFound];

                        $returnValues = new ReturnController("3000", "SUCCESS", $data);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $listDocument = Document::orderBy('id','desc')->paginate(15);

                        foreach ($listDocument as $document)
                        {
                            $tempArray = [];
                            $tempArray['documentId'] = $document['documentId'];
                            $tempArray['documentTitle'] = $document['documentTitle'];
                            $tempArray['documentPath'] = $DEFAULT_PATH.$document['documentPath'];
                            array_push($listDocumentFound,$tempArray);
                        }
                        $data=[
                            "total" => $listDocument->total(),
                            "nextPageUrl" => $listDocument->nextPageUrl(),
                            "previousPageUrl" => $listDocument->previousPageUrl(),
                            "currentPage" => $listDocument->currentPage(),
                            "lastPage" => $listDocument->lastPage(),
                            "data" => $listDocumentFound
                        ];
                        $returnValues = new ReturnController("3000","SUCCESS",$data);
                        $return = $returnValues->returnValues();
                        return $return;

                    }
                break;

            }
        }
    }
    public function delete(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $DEFAULT_PATH = $getDefaultVariables->getDefaultPaths();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $checkDocument =  Document::where('documentId',$request->input('documentId'))->first();

            if(count($checkDocument) <=0)
            {
                $returnValues = new ReturnController("9002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $deleteDocument = Document::where('documentId',$request->input('documentId'))->delete();

                if($deleteDocument < 0)
                {
                    $returnValues = new ReturnController("9002", "FAILURE", "");
                    $return = $returnValues->returnValues();
                    return $return;

                }
                else
                {
                    $filename =  $checkDocument['documentPath'];
                    $documentId = $checkDocument['documentId'];
                    $path = public_path();
                    $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $documentPath =  $path.'/sfgc/documents/'.$documentId.".".$file_ext;
                    unlink($documentPath);
                    $returnValues = new ReturnController("9000", "SUCCESS", "");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

}
