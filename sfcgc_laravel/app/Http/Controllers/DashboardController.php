<?php

namespace App\Http\Controllers;

use App\AppBuild;
use App\AppInfo;
use App\Document;
use App\FlashNews;
use App\Gallery;
use App\Http\Middleware\AuthUser;
use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    private $totalDocumentCount;
    private $totalFlashNewsCount;
    private $totalImageCount;

    public function getData(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == "400")
        {

            $this->totalDocumentCount = $this->getDocumentCount($request);
            $this->totalFlashNewsCount = $this->getFlashNewsCount($request);
            $this->totalImageCount = $this->getImageCount($request);

            $dashboardData = ["totalDocumentCount" => $this->totalDocumentCount,
                "totalFlashNewsCount" => $this->totalFlashNewsCount,
                'totalImageCount' => $this->totalImageCount];

            $returnValues = new ReturnController("8000","SUCCESS",$dashboardData);
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getDocumentCount(Request $request)
    {
        $documentCount = Document::count();
         return $documentCount;
    }
    public function getFlashNewsCount(Request $request)
    {
        $flashNewsCount = FlashNews::count();
        return $flashNewsCount;
    }
    public function getImageCount(Request $request)
    {
        $galleryCount = Gallery::count();
        return $galleryCount;
    }
}
