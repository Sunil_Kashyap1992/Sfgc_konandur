<?php

namespace App\Http\Controllers;

use App\FlashNews;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use Illuminate\Http\Request;

use App\Http\Requests;

class FlashNewsController extends Controller
{
    public function add(Request $request)
    {

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {

            $path = public_path();

            $generateUniqueId = new GenerateUUID();
            $flashNewsId = $generateUniqueId->getUniqueId();

            $addFlashNews = new FlashNews();
            $addFlashNews->flashNewsId = $flashNewsId;
            $addFlashNews->flashNewsTitle = $request->input('flashNewsTitle');
            $addFlashNews->save();

            if(!$addFlashNews->save())
            {
                $returnValues = new ReturnController("4002", "FAILURE", "");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $returnValues = new ReturnController("4000", "SUCCESS", "");
                $return = $returnValues->returnValues();
                return $return;

            }


        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $listFlashNews = FlashNews::orderBy('id','desc')->get();

        if(count($listFlashNews) <=0)
        {
            $returnValues = new ReturnController("5002", "FAILURE", "");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $listFlashNewsFound = [];

            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    if(count($listFlashNews) <= $paginationCount)
                    {
                        foreach ($listFlashNews as $flashNews)
                        {
                            $tempArray = [];
                            $tempArray['flashNewsId'] = $flashNews['flashNewsId'];
                            $tempArray['flashNewsTitle'] = $flashNews['flashNewsTitle'];
                            array_push($listFlashNewsFound,$tempArray);
                        }
                        $data = [
                            "lastPage" => "NULL",
                            "data" => $listFlashNewsFound];

                        $returnValues = new ReturnController("5000", "SUCCESS", $data);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $listFlashNews = FlashNews::orderBy('id','desc')->paginate($paginationCount);

                        foreach ($listFlashNews as $flashNews)
                        {
                            $tempArray = [];
                            $tempArray['flashNewsId'] = $flashNews['flashNewsId'];
                            $tempArray['flashNewsTitle'] = $flashNews['flashNewsTitle'];
                            array_push($listFlashNewsFound,$tempArray);
                        }
                        $data=[
                            "total" => $listFlashNews->total(),
                            "nextPageUrl" => $listFlashNews->nextPageUrl(),
                            "previousPageUrl" => $listFlashNews->previousPageUrl(),
                            "currentPage" => $listFlashNews->currentPage(),
                            "lastPage" => $listFlashNews->lastPage(),
                            "data" => $listFlashNewsFound
                        ];
                        $returnValues = new ReturnController("5000","SUCCESS",$data);
                        $return = $returnValues->returnValues();
                        return $return;

                    }
                    break;
                case "1":
                    $listFlashNews = FlashNews::orderBy('id','desc')->first();

                    if(count($listFlashNews) <=0)
                    {
                        $returnValues = new ReturnController("5002", "FAILURE", "");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("5000", "SUCCESS", $listFlashNews['flashNewsTitle']);
                        $return = $returnValues->returnValues();
                        return $return;
                        break;

                    }

            }
        }
    }
}
