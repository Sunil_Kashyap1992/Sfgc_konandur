<?php

namespace App\Http\Middleware;

use Closure;

class Configuration
{
    public function getDefaultPaths()
    {
        $DEV_MODE = 1;
        $DEV_PATH = "http://localhost:8000/";
        $DEFAULT_PATH = "http://sfgckonandur.in/";

        if ($DEV_MODE == 1) {
            $DEFAULT_PATH = $DEV_PATH;
        }
        return $DEFAULT_PATH;
    }
    public function getPaginationcount()
    {
        $paginationCount = 10;
        return $paginationCount;
    }
}
