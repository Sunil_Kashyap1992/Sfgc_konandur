app
.controller('DashboardController',function($scope,$http){
	$scope.authToken = localStorage.getItem("sfgc_token");
	$scope.listDashboard=function(){
		$scope.ajaxPromise = $http({
			method : "GET",
			url : BASE_URL+LIST_DASHBOARD_URL,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_DASHBOARD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
					$scope.totalDocumentCount = response.data.data.totalDocumentCount;
					$scope.totalFlashNewsCount = response.data.data.totalFlashNewsCount;
					$scope.totalImageCount = response.data.data.totalImageCount;
				}
				else{
					console.log("no data found");
					$scope.totalDocumentCount = 0;
					$scope.totalFlashNewsCount = 0;
					$scope.totalImageCount = 0;
				}
			}
			else{
				console.log("no data found");
				$scope.totalDocumentCount = 0;
				$scope.totalFlashNewsCount = 0;
				$scope.totalImageCount = 0;
			}
		},function failure(){
			console.log("no data found");
			$scope.totalDocumentCount = 0;
			$scope.totalFlashNewsCount = 0;
			$scope.totalImageCount = 0;
		});
	}
})