
var GalleryFormData = new FormData();
//service to upload files
app
.factory('GalleryUploadService', ['$http','$q', function ($http,$q) {
    var galleryUploadService = {};
    galleryUploadService.addImage = function(authToken,uploadUrl){
        var deferred = $q.defer();
        return $http.post(uploadUrl, GalleryFormData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
                token : authToken,
            }
        }).then(function success(response){
            console.log(JSON.stringify(response));
            if(response.data.errorCode == GALLERY_ADD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                notify('Image Added', 'success');
                deferred.resolve(response);
                return deferred.promise;
            }
            else{
                notify('Image Cannot be added', 'danger');
                deferred.reject();
                return deferred.promise;
            }
        },function failure(){
            notify('Image Cannot be added', 'danger');
            deferred.reject();
            return deferred.promise;
        })
    }
    return galleryUploadService;
}])

app
.controller('GalleryController',function($scope,$http,GalleryUploadService){
	$scope.authToken = localStorage.getItem("sfgc_token");

	$scope.isGalleryFormOpened = false;
	$scope.toggleGalleryForm = function(){
		if($scope.isGalleryFormOpened == false){
			$("#gallery").slideDown("slow");
			$("#closeGallery").slideUp("slow");
			$scope.isGalleryFormOpened = true;
		}
		else{
			$("#closeGallery").slideDown("slow");
			$("#gallery").slideUp("slow");
			$scope.isGalleryFormOpened = false;
		}
	};	

	$scope.listAllImages =function(paginateFlag,url){
        if(paginateFlag == 0){
            $scope.galleryListUrl = BASE_URL+LIST_GALLERY_URL;
        }
        else{
            $scope.galleryListUrl = url;
        }
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.galleryListUrl,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_GALLERY_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.lastPageForGallery = response.data.data.lastPage;
                    $scope.imagesPresent =true;
                    $scope.showPaginationForGallery = true;
                    if($scope.lastPageForGallery == "NULL"){
                        $scope.showPaginationForGallery = false;
                        $scope.images=response.data.data.data;
                    }
                    else{
                        $scope.images=response.data.data.data;
                    }
                    $scope.previousPageUrlForGallery = response.data.data.previousPageUrl;
                    $scope.nextPageUrlForGallery = response.data.data.nextPageUrl;
                    $scope.currentPageForGallery = response.data.data.currentPage;
				}
				else{
					console.log("no data found");
                    $scope.imagesPresent =false;
				}
			}
			else{
				console.log("no data found");
                $scope.imagesPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.imagesPresent =false;
		});
	}

    $scope.addImageFunction = function(){
        GalleryFormData.append('gallery',$("#galleryImage").prop('files')[0]);
        $scope.uploadUrl = BASE_URL+GALLERY_ADD_URL;
        GalleryUploadService.addImage($scope.authToken,$scope.uploadUrl).then(function success(response){
            $scope.listAllImages(0,'');
            $scope.toggleGalleryForm();
            document.getElementById('galleryImage').value = "";
            GalleryFormData.delete('gallery');
        },function failure(){
            $scope.listAllImages(0,'');
            $scope.toggleGalleryForm();
            document.getElementById('galleryImage').value = "";
            GalleryFormData.delete('gallery');
        })
    };
})