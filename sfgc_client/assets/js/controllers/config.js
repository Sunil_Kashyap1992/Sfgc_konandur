var BASE_URL = "http://sfgckonandur.in/";
var LOGIN_URL = "login/authenticate";
var SUCCESS_STATUS_TEXT = "SUCCESS";
var FAILURE_STATUS_TEXT = "FAILURE";
var GALLERY_ADD_URL = "gallery/add";
var LIST_GALLERY_URL = "gallery/listAll";
var LIST_DASHBOARD_URL = "dashboard/getData";
var DOCUMENT_ADD_URL = "document/add";
var LIST_DOCUMENTS_URL = "document/listAll";
var LIST_NEWS_URL = "flashNews/listAll";
var ADD_NEWS_URL = "flashNews/add";
var LIST_DOCUMENTS_SUCCESS_CODE = 3000;
var DOCUMENT_ADD_SUCCESS_CODE = 2000;
var HTTP_STATUS_CODE = 200;
var LOGIN_SUCCESS_CODE = 1000;
var LIST_GALLERY_SUCCESS_CODE = 7000;
var GALLERY_ADD_SUCCESS_CODE = 6000;
var LIST_DASHBOARD_SUCCESS_CODE = 8000;
var ADD_NEWS_SUCCESS_CODE = 4000;
var LIST_NEWS_SUCCESS_CODE = 5000;
