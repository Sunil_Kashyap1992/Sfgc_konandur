
var DocumentFormData = new FormData();
//service to upload files
app
.factory('DocumentUploadService', ['$http','$q', function ($http,$q) {
    var documentUploadService = {};
    documentUploadService.addDocument = function(authToken,documentData,uploadUrl){
        var deferred = $q.defer();
        return $http.post(uploadUrl, DocumentFormData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
                token : authToken,
                documentTitle : documentData.documentTitle
            }
        }).then(function success(response){
            console.log(JSON.stringify(response));
            if(response.data.errorCode == DOCUMENT_ADD_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                notify('Document Added', 'success');
                deferred.resolve(response);
                return deferred.promise;
            }
            else{
                notify('Document Cannot be added', 'danger');
                deferred.reject();
                return deferred.promise;
            }
        },function failure(){
            notify('Document Cannot be added', 'danger');
            deferred.reject();
            return deferred.promise;
        })
    }
    return documentUploadService;
}])

app
.controller('DocumentsController',function($scope,$http,DocumentUploadService,$sce){
	$scope.authToken = localStorage.getItem("sfgc_token");

	$scope.isDocumentFormOpened = false;
	$scope.toggleDocumentForm = function(){
		if($scope.isDocumentFormOpened == false){
			$("#document").slideDown("slow");
			$("#closeDocument").slideUp("slow");
			$scope.isDocumentFormOpened = true;
		}
		else{
			$("#closeDocument").slideDown("slow");
			$("#document").slideUp("slow");
			$scope.isDocumentFormOpened = false;
		}
	};	

	$scope.listAllDocuments =function(paginateFlag,url){
        if(paginateFlag == 0){
            $scope.documentListUrl = BASE_URL+LIST_DOCUMENTS_URL;
        }
        else{
            $scope.documentListUrl = url;
        }
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.documentListUrl,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
            console.log(JSON.stringify(response));
			if(response.status == HTTP_STATUS_CODE){
				if(response.data.errorCode == LIST_DOCUMENTS_SUCCESS_CODE && response.data.statusText == SUCCESS_STATUS_TEXT){
                    $scope.lastPageForDocument = response.data.data.lastPage;
                    $scope.documentsPresent =true;
                    $scope.showPaginationForDocument = true;
                    if($scope.lastPageForDocument == "NULL"){
                        $scope.showPaginationForDocument = false;
                        $scope.documents=response.data.data.data;
                    }
                    else{
                        $scope.documents=response.data.data.data;
                    }
                    $scope.previousPageUrlForDocument = response.data.data.previousPageUrl;
                    $scope.nextPageUrlForDocument = response.data.data.nextPageUrl;
                    $scope.currentPageForDocument = response.data.data.currentPage;
                    for(i=0;i<$scope.documents.length;i++){
                        $scope.documents[i].documentPath = $sce.trustAsResourceUrl(String($scope.documents[i].documentPath));
                    }
				}
				else{
					console.log("no data found");
                    $scope.documentsPresent =false;
				}
			}
			else{
				console.log("no data found");
                $scope.documentsPresent =false;
			}
		},function failure(){
			console.log("no data found");
            $scope.documentsPresent =false;
		});
	}

    $scope.addDocumentFunction = function(documentObject){
        DocumentFormData.append('document',$("#documentFile").prop('files')[0]);
        $scope.uploadUrl = BASE_URL+DOCUMENT_ADD_URL;
        DocumentUploadService.addDocument($scope.authToken,documentObject,$scope.uploadUrl).then(function success(response){
            $scope.listAllDocuments(0,'');
            $scope.toggleDocumentForm();
            document.getElementById('documentFile').value = "";
            DocumentFormData.delete('document');
        },function failure(){
            $scope.listAllDocuments(0,'');
            $scope.toggleDocumentForm();
            document.getElementById('documentFile').value = "";
            DocumentFormData.delete('document');
        })
    };

    $scope.openDocument = function(document){
        var aTag = document.createElement("a");
        aTag.setAttribute('href', document.documentPath);
    }
})