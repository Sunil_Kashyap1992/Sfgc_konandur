<?php
    include_once 'plugins.php';
    include_once 'sidebar.php';
?>
<!DOCTYPE html>
    <head>
        <title>SFGC</title>
        <?php echo loadPlugins()?>
    </head>
    <body ng-app="sfgc" class="ng-cloak" ng-controller="GalleryController" ng-init="listAllImages(0,'')">
        <header id="header" class="ng-cloak">
            <ul class="header-inner ng-cloak">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>
                <li class="logo hidden-xs">
                    <a href="projects.php">Gallery Images</a>
                </li>
            </ul>
            <div id="top-search-wrap">
                <input type="text">
                <i id="top-search-close">&times;</i>
            </div>
        </header>
        <section id="main" class="ng-cloak">
            <aside id="sidebar" class="ng-cloak">
                <?php echo loadSidebar()?>
            </aside>        
            <section id="content" class="ng-cloak">
                <div class="container ng-cloak">
                    <div class="block-header">
                        <h2>Gallery</h2>
                        <ul class="actions" style="margin-right: 5%">
                            <li>
                                <a href="">
                                    <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleGalleryForm()"><i class="fa fa-plus"></i> Add Image</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <br/>
                    <div class="dash-widgets ng-cloak" id="closeGallery">
                        <div ng-if="imagesPresent == true">
                            <div class="row media ng-cloak">
                                <div class="col-md-3 col-sm-6 ng-cloak" ng-repeat="image in images" style="background-color: white;margin-right: 5px;margin-bottom: 5px">
                                    <img ng-src={{image.imagePath}} style="height: 100%;width: 100%">
                                </div>
                            </div>
                        </div>
                        <div class="text-center ng-cloak" ng-show="showPaginationForGallery">
                            <br/>
                            <div class="btn-group">
                                <button ng-disabled="currentPageForGallery == 1" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllImages(1,previousPageUrlForGallery)"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-primary ng-cloak" max-size="maxSize" boundary-links="true">{{currentPageForGallery}}/{{lastPageForGallery}}</button>
                                <button ng-disabled="currentPageForGallery == lastPageForGallery" class="btn btn-white" max-size="maxSize" boundary-links="true" ng-click="listAllImages(1,nextPageUrlForGallery)"><i class="fa fa-chevron-right"></i></button>
                            </div>
                        </div>
                        <div class="text-center ng-cloak" ng-show="showPaginationForGallery">
                            <br/>
                        </div>
                        <div class="card" ng-if="imagesPresent == false">
                            <div class="card-header ch-alt text-center">
                                <i class="zmdi zmdi-book-image fa-4x"></i>
                            </div>
                            <div class="card-body card-padding text-center">
                                <h2>No Images Present</h2>
                            </div>
                        </div>
                    </div>
                    <div class="card ng-cloak" id="gallery" style="display: none">
                        <div class="card-header">
                            <h2>Add Image</h2>
                        </div>

                        <div class="card-body card-padding">
                            <form name = "addImage">
                                <input type="file" class="form-control custom-input-form" placeholder="galleryImage" file-model="gallery.galleryImage" name="galleryImage" id="galleryImage" value="Image" accept="image/x-png,image/gif,image/jpeg">
                                <br/>
                                <button class="btn btn-default waves-effect" ng-click="toggleGalleryForm()">Close</button>
                                <button class="btn btn-primary waves-effect" ng-click="addImageFunction()">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <footer id="footer">
            <?php echo loadFooter()?>
            <div id="timer">
            </div>
        </footer>
        <?php echo loadAfterScripts() ?>
    </body>
  </html>