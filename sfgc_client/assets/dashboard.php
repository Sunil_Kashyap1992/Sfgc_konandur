<?php
    include_once 'plugins.php';
    include_once 'sidebar.php';
?>
<!DOCTYPE html>
    <head>
        <title>SFGC</title>
        <?php echo loadPlugins()?>
    </head>
    <body ng-app="sfgc" class="ng-cloak" ng-controller="DashboardController" ng-init="listDashboard()">
        <header id="header" class="ng-cloak">
            <ul class="header-inner ng-cloak">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>
                <li class="logo hidden-xs">
                    <a href="dashboard.php">Dashboard</a>
                </li>
            </ul>
            <div id="top-search-wrap">
                <input type="text">
                <i id="top-search-close">&times;</i>
            </div>
        </header>
        <section id="main" class="ng-cloak">
            <aside id="sidebar" class="ng-cloak">
                <?php echo loadSidebar()?>
            </aside>        
            <section id="content" class="ng-cloak">
                <div class="container ng-cloak">
                    <div class="block-header ng-cloak">
                        <h2>Dashboard</h2>
                    </div>
                    <div class="dash-widgets ng-cloak">
                        <div class="row">
                            <a href="documents.php">
                                <div class="col-md-3 col-sm-6">
                                    <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                        <div class="bgm-pink">
                                            <div class="text-center p-20 m-t-25">
                                                <div class="easy-pie main-pie">
                                                    <div class="percent percent-without-symbol">{{totalDocumentCount}}</div>
                                                    <div class="pie-title">Documents</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="flashNews.php">
                                <div class="col-md-3 col-sm-6">
                                    <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                        <div class="bgm-amber">
                                            <div class="text-center p-20 m-t-25">
                                                <div class="easy-pie main-pie">
                                                    <div class="percent percent-without-symbol">{{totalFlashNewsCount}}</div>
                                                    <div class="pie-title">Flash News</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="gallery.php">
                                <div class="col-md-3 col-sm-6">
                                    <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                        <div class="bgm-blue">
                                            <div class="text-center p-20 m-t-25">
                                                <div class="easy-pie main-pie">
                                                    <div class="percent percent-without-symbol">{{totalImageCount}}</div>
                                                    <div class="pie-title">Images</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        
        <footer id="footer">
            <?php echo loadFooter()?>
            <div id="timer">
            </div>
        </footer>
        <?php echo loadAfterScripts() ?>
    </body>
  </html>